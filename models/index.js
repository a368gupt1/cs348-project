const fs = require('fs');
const { Sequelize } = require('sequelize');
const db = {};

// REPLACE ENV VARIABLES WITH YOUR MYSQL INFO
const sequelize = new Sequelize(
	process.env.MYSQL_DATABASE,
	process.env.MYSQL_USERNAME,
	process.env.MYSQL_PASSWORD,
	{
		host: 'localhost',
		dialect: 'mysql',
		// Change this to true if wanna show queries and arguments
		// logging: console.log,
		logQueryParameters: true,
		logging: false,
		// logging: (msg) => {
		// 	var stream = fs.createWriteStream('test-sample.sql', { flags: 'a' });
		// 	stream.write(msg + '\n');
		// 	stream.end();
		// },
	}
);

db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;

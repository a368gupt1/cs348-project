const dropMovies = `DROP TABLE IF EXISTS movies;`;

const createMovies = `CREATE TABLE IF NOT EXISTS movies (
	uuid CHAR(36)BINARY,
	title VARCHAR(255) NOT NULL,
	overview TEXT NOT NULL,
	poster VARCHAR(255),
	backdrop VARCHAR(255),
	adult TINYINT(1) NOT NULL,
	releasedDate DATE NOT NULL,
	runtime INTEGER NOT NULL,
	ratingCount INTEGER NOT NULL,
	ratingAverage DECIMAL(5,4) NOT NULL,
	popularity INTEGER NOT NULL,
	PRIMARY KEY (uuid)
);`;

const movieTitleIndex = `CREATE INDEX movieTitle ON movies (title);`;
const moviePopularityIndex = `CREATE INDEX moviePopularity ON movies (popularity);`;
const movieRatingAverageIndex = `CREATE INDEX movieRatingAverage ON movies (ratingAverage);`;

const insertReviewTrigger = `CREATE TRIGGER update_movie_rating AFTER INSERT ON reviews
FOR EACH ROW
BEGIN
  UPDATE movies SET
    ratingCount = ratingCount + 1,
    ratingAverage = ROUND(((ratingAverage * ratingCount) + NEW.rating) / (ratingCount + 1), 4)
  WHERE uuid = NEW.movieUuid;
END;
`;

const deleteReviewTrigger = `CREATE TRIGGER update_movie_rating_after_delete AFTER DELETE ON reviews
FOR EACH ROW
BEGIN
  UPDATE movies
  SET ratingCount = ratingCount - 1,
      ratingAverage = ROUND(
        (ratingAverage * ratingCount - OLD.rating) / (ratingCount - 1),
        4
      )
  WHERE uuid = OLD.movieUuid;
END;
`;

module.exports = {
	dropMovies,
	createMovies,
	movieTitleIndex,
	moviePopularityIndex,
	movieRatingAverageIndex,
	insertReviewTrigger,
	deleteReviewTrigger,
};

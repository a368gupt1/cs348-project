const dropReviews = `DROP TABLE IF EXISTS reviews;`;

const createReviews = `CREATE TABLE IF NOT EXISTS reviews (
	uuid CHAR(36)BINARY,
	content TEXT NOT NULL,
	rating INTEGER NOT NULL,
	movieUuid CHAR(36)BINARY,
	userUuid CHAR(36)BINARY,
	PRIMARY KEY (uuid),
	FOREIGN KEY (movieUuid)
			REFERENCES movies (uuid)
			ON DELETE CASCADE ON UPDATE CASCADE,
	FOREIGN KEY (userUuid)
			REFERENCES users (uuid)
			ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT check_rating CHECK (rating BETWEEN 1 AND 10)
);`;

module.exports = {
	dropReviews,
	createReviews,
};

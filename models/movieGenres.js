const dropMovieGenres = `DROP TABLE IF EXISTS movieGenres;`;

const createMovieGenres = `CREATE TABLE IF NOT EXISTS movieGenres (
  genreUuid CHAR(36)BINARY,
  movieUuid CHAR(36)BINARY,
  PRIMARY KEY (genreUuid , movieUuid),
  FOREIGN KEY (genreUuid)
      REFERENCES genres (uuid)
      ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY (movieUuid)
      REFERENCES movies (uuid)
      ON DELETE CASCADE ON UPDATE CASCADE
);`;

module.exports = {
	dropMovieGenres,
	createMovieGenres,
};

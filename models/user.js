const dropUsers = `DROP TABLE IF EXISTS users;`;

const createUsers = `CREATE TABLE IF NOT EXISTS users (
	uuid CHAR(36)BINARY,
	username VARCHAR(255) NOT NULL,
	email VARCHAR(255) UNIQUE NOT NULL,
	role VARCHAR(255) NOT NULL,
	profile TEXT NOT NULL,
	password VARCHAR(255) NOT NULL,
	PRIMARY KEY (uuid),
  CONSTRAINT check_email CHECK (email REGEXP '^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,}$'),
	CONSTRAINT check_role CHECK (role IN ('Admin', 'User'))
);`;

module.exports = {
	createUsers,
	dropUsers,
};

const dropFavourites = `DROP TABLE IF EXISTS favourites;`;

const createFavourites = `CREATE TABLE IF NOT EXISTS favourites (
	userUuid CHAR(36)BINARY,
	movieUuid CHAR(36)BINARY,
	FOREIGN KEY (userUuid)
			REFERENCES users (uuid)
			ON DELETE CASCADE ON UPDATE CASCADE,
	FOREIGN KEY (movieUuid)
			REFERENCES movies (uuid)
			ON DELETE CASCADE ON UPDATE CASCADE,
  PRIMARY KEY (userUuid, movieUuid)
);`;

module.exports = {
	dropFavourites,
	createFavourites,
};

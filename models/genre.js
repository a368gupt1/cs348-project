const dropGenres = `DROP TABLE IF EXISTS genres;`;

const createGenres = `CREATE TABLE IF NOT EXISTS genres (
	uuid CHAR(36)BINARY,
	name VARCHAR(255) UNIQUE NOT NULL,
	PRIMARY KEY (uuid)
);`;

const genreIndex = `CREATE INDEX genreName ON genres (name);`;

module.exports = {
	dropGenres,
	createGenres,
	genreIndex,
};

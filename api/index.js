require('dotenv').config();
const fs = require('fs');
const _ = require('lodash');

const getImagePath = (poster) => {
	return poster ? `http://image.tmdb.org/t/p/original${poster}` : null;
};

const getMovieDetails = async (ids) => {
	const movies = [];
	const genres = [];
	for (const id of ids) {
		const movieResponse = await fetch(
			`https://api.themoviedb.org/3/movie/${id}?api_key=${process.env.API_KEY}&language=${process.env.LANGUAGE}`
		);
		const {
			title,
			overview,
			poster_path,
			backdrop_path,
			adult,
			release_date: releasedDate,
			runtime,
			vote_count: ratingCount,
			vote_average: ratingAverage,
			popularity,
			genres: movieGenres,
		} = await movieResponse.json();
		const movie = {
			title,
			overview,
			poster: getImagePath(poster_path),
			backdrop: getImagePath(backdrop_path),
			adult,
			releasedDate,
			genres: movieGenres.map((genre) => genre.name),
			runtime,
			ratingCount,
			ratingAverage: ratingAverage.toFixed(4),
			popularity: Math.floor(popularity),
		};
		movieGenres.forEach((genre) => {
			genres.push({
				name: genre.name,
			});
		});
		movies.push(movie);
	}
	return {
		movies,
		genres: _.unionBy(genres, 'name'),
	};
};

const getMovieIds = async (maxPage = 10) => {
	let ids = [];
	for (let page = 1; page <= maxPage; page++) {
		const res = await fetch(
			`https://api.themoviedb.org/3/movie/popular?api_key=${process.env.API_KEY}&language=${process.env.LANGUAGE}&page=${page}`
		);
		const { results } = await res.json();
		ids = [...ids, ...results.map((result) => result.id)];
	}
	return ids;
};

const getData = async (maxPage = 10) => {
	const ids = await getMovieIds(maxPage);
	const { genres, movies } = await getMovieDetails(ids);
	// fs.writeFileSync('./data/genres.json', JSON.stringify(genres));
	fs.writeFileSync('./data/movies-production.json', JSON.stringify(movies));
	// console.log('Updated data files');
};
getData(100);

module.exports = { getData };

const StatusError = require('../errors');
const {
	sanitizeInput,
	executeSelectQuery,
	getObjectValue,
	executeInsertQuery,
	sliceUuid,
} = require('../repositories/utilRepo');
const { selectGenresStats } = require('./queries/genreQueries');

const FIELDS = ['name'];

const getGenreById = async (uuid) => {
	try {
		const sanitizedUuid = sanitizeInput(uuid);
		const genre = (
			await executeSelectQuery(
				`SELECT * FROM genres WHERE uuid = ${sanitizedUuid};`
			)
		)[0];
		if (!genre) {
			throw new StatusError([{ msg: "Genre doesn't exist" }], 404);
		}
		return genre;
	} catch (error) {
		throw new StatusError([{ msg: error.message }], 500);
	}
};

const getGenreByName = async (name) => {
	try {
		const sanitizedName = sanitizeInput(name);
		const genre = (
			await executeSelectQuery(
				`SELECT * FROM genres WHERE name = ${sanitizedName};`
			)
		)[0];
		if (!genre) {
			throw new StatusError([{ msg: "Genre doesn't exist" }], 404);
		}
		return genre;
	} catch (error) {
		throw new StatusError([{ msg: error.message }], 500);
	}
};

const getAllGenres = async () => {
	try {
		const query = selectGenresStats();
		const response = await executeSelectQuery(query);
		const responseData = {
			labels: [],
			moviesCount: [],
			avgRating: [],
			minRating: [],
			maxRating: [],
		};
		response.forEach((genre) => {
			responseData.labels.push(genre.name);
			responseData.avgRating.push(genre.avgRating);
			responseData.minRating.push(genre.minRating);
			responseData.maxRating.push(genre.maxRating);
			responseData.moviesCount.push(genre.moviesCount);
		});
		return responseData;
	} catch (error) {
		throw new StatusError([{ msg: error.message }], 500);
	}
};

const insertGenre = async (genre) => {
	try {
		const { fields, value } = getObjectValue(genre, FIELDS);
		const query = `INSERT INTO genres (${fields}) VALUES (${value});`;
		await executeInsertQuery(query);
		genre.uuid = sliceUuid(value[0]);
		return genre;
	} catch (error) {
		throw new StatusError([{ msg: error.message }], 500);
	}
};

const insertGenres = async (genres) => {
	try {
		const fields = ['uuid', ...FIELDS];
		const values = [];
		genres.forEach((genre) => {
			const { value } = getObjectValue(genre, FIELDS);
			values.push(`(${value})`);
		});
		const query = `INSERT INTO genres (${fields}) VALUES ${values};`;
		return await executeInsertQuery(query);
	} catch (error) {
		throw new StatusError([{ msg: error.message }], 500);
	}
};

module.exports = {
	getGenreById,
	getGenreByName,
	getAllGenres,
	insertGenre,
	insertGenres,
};

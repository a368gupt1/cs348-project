const StatusError = require('../errors');
const {
	executeSelectQuery,
	sanitizeInputs,
	executeInsertQuery,
	executeDeleteQuery,
} = require('../repositories/utilRepo');

const hasFavourited = async (favourite) => {
	const { userUuid, movieUuid } = sanitizeInputs(favourite, [
		'userUuid',
		'movieUuid',
	]);
	const query = `SELECT * FROM favourites WHERE userUuid = ${userUuid} AND movieUuid = ${movieUuid};`;
	try {
		const favourite = (await executeSelectQuery(query))[0];
		return !!favourite;
	} catch (error) {
		throw new StatusError([{ msg: error.message }], 500);
	}
};

const insertFavourite = async (favourite) => {
	const { userUuid, movieUuid } = sanitizeInputs(favourite, [
		'userUuid',
		'movieUuid',
	]);
	try {
		const query = `INSERT INTO favourites(userUuid, movieUuid) VALUES (${userUuid}, ${movieUuid});`;
		return await executeInsertQuery(query);
	} catch (error) {
		throw new StatusError([{ msg: error.message }], 500);
	}
};

const deleteFavourite = async (favourite) => {
	const { userUuid, movieUuid } = sanitizeInputs(favourite, [
		'userUuid',
		'movieUuid',
	]);
	try {
		const query = `DELETE FROM favourites WHERE userUuid = ${userUuid} AND movieUuid = ${movieUuid};`;
		return await executeDeleteQuery(query);
	} catch (error) {
		throw new StatusError([{ msg: error.message }], 500);
	}
};

module.exports = {
	insertFavourite,
	hasFavourited,
	deleteFavourite,
};

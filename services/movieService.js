const StatusError = require('../errors');
const {
	executeSelectQuery,
	getSelectFields,
	executeInsertQuery,
	sanitizeInput,
	sanitizeInputs,
	getObjectValue,
	executeDeleteQuery,
	sliceUuid,
} = require('../repositories/utilRepo');
const {
	selectSuggestedMovies,
	selectTrendingMovies,
	selectPopularMovies,
	selectMovieWithId,
	selectFavoriteMovies,
} = require('./queries/movieQueries');

const LIMIT = 19;

const FIELDS = [
	'title',
	'overview',
	'poster',
	'backdrop',
	'adult',
	'releasedDate',
	'runtime',
	'ratingCount',
	'ratingAverage',
	'popularity',
];

//#region Helper functions
const getMoviesQuery = (movies, limit) => {
	let hasMore = false;
	if (movies.length === limit) {
		movies.pop();
		hasMore = true;
	}
	return { movies, hasMore };
};

const getMoviesFromResult = (queryResult) => {
	const movieMap = {};
	queryResult.forEach((movie) => {
		if (movieMap[movie.uuid]) {
			movieMap[movie.uuid].genres.push({
				uuid: movie.GenreUuid,
				name: movie.name,
			});
		} else {
			movieMap[movie.uuid] = {
				uuid: movie.uuid,
				title: movie.title,
				overview: movie.overview,
				poster: movie.poster,
				backdrop: movie.backdrop,
				adult: movie.adult,
				releasedDate: movie.releasedDate,
				runtime: movie.runtime,
				ratingCount: movie.ratingCount,
				ratingAverage: movie.ratingAverage,
				popularity: movie.popularity,
				genres: movie.GenreUuid
					? [
							{
								uuid: movie.GenreUuid,
								name: movie.name,
							},
					  ]
					: [],
				matchingGenres: movie.matchingGenres,
			};
		}
	});
	return Object.values(movieMap);
};

const getLimit = (limit) => {
	return limit ? parseInt(limit, 10) : LIMIT;
};
//#endregion

const getMovieById = async (uuid) => {
	const sanitizedUuid = sanitizeInput(uuid);
	const reviews = await executeSelectQuery(
		`SELECT * FROM reviews WHERE movieUuid=${sanitizedUuid};`
	);
	const favourites = await executeSelectQuery(
		`SELECT * FROM favourites WHERE movieUuid=${sanitizedUuid};`
	);
	const query = selectMovieWithId(sanitizedUuid);
	const movie = (await executeSelectQuery(query))[0];
	if (!movie) {
		throw new StatusError([{ msg: "Movie doesn't exist" }], 404);
	}
	movie.reviews = reviews;
	movie.favourites = favourites;
	return movie;
};

const getMoviesByName = async (title) => {
	// if (!title) {
	// 	throw new StatusError([{ msg: 'Empty title' }], 404);
	// }
	try {
		const sanitizeTitle = sanitizeInput(`%${title.toLowerCase()}%`);
		return await executeSelectQuery(
			`SELECT uuid AS value, title AS label FROM movies WHERE LOWER(title) LIKE ${sanitizeTitle} LIMIT ${LIMIT};`
		);
	} catch (error) {
		throw new StatusError([{ msg: error.message }], 500);
	}
};

/**
 * @param query req.query
 */
const getPopularMovies = async (query) => {
	let { sort, limit, popularity, uuid } = sanitizeInputs(
		query,
		['sort', 'limit', 'popularity', 'uuid'],
		['sort', 'limit']
	);
	const moviesLimit = getLimit(limit);
	sort = sort !== 'ASC' ? 'DESC' : 'ASC';
	const compare = sort === 'DESC' ? '<' : '>';
	const attributes = {
		temp: [
			'uuid',
			'title',
			'overview',
			'poster',
			'backdrop',
			'adult',
			'releasedDate',
			'runtime',
			'ratingCount',
			'ratingAverage',
			'popularity',
		],
		genres: ['name'],
		movieGenres: ['GenreUuid'],
	};
	const fields = getSelectFields(attributes);
	const sqlQuery = selectPopularMovies({
		sort,
		compare,
		moviesLimit,
		popularity,
		uuid,
		fields,
	});
	try {
		const result = await executeSelectQuery(sqlQuery);
		const movies = getMoviesFromResult(result);
		return getMoviesQuery(movies, moviesLimit);
	} catch (error) {
		throw new StatusError([{ msg: error.message }], 500);
	}
};

const getTrendingMovies = async (query) => {
	let { sort, limit, ratingAverage, uuid } = sanitizeInputs(
		query,
		['sort', 'limit', 'ratingAverage', 'uuid'],
		['sort', 'limit']
	);
	const moviesLimit = getLimit(limit);
	sort = sort !== 'ASC' ? 'DESC' : 'ASC';
	const compare = sort === 'DESC' ? '<' : '>';
	const attributes = {
		temp: [
			'uuid',
			'title',
			'overview',
			'poster',
			'backdrop',
			'adult',
			'releasedDate',
			'runtime',
			'ratingCount',
			'ratingAverage',
			'popularity',
		],
		genres: ['name'],
		movieGenres: ['GenreUuid'],
	};
	const fields = getSelectFields(attributes);
	const sqlQuery = selectTrendingMovies({
		sort,
		compare,
		moviesLimit,
		ratingAverage,
		uuid,
		fields,
	});
	try {
		const result = await executeSelectQuery(sqlQuery);
		const movies = getMoviesFromResult(result);
		return getMoviesQuery(movies, moviesLimit);
	} catch (error) {
		throw new StatusError([{ msg: error.message }], 500);
	}
};

const getSuggestedMovies = async (query) => {
	let { sort, limit, matchingGenres, uuid, userUuid } = sanitizeInputs(
		query,
		['sort', 'limit', 'matchingGenres', 'uuid', 'userUuid'],
		['sort', 'limit']
	);
	const moviesLimit = getLimit(limit);
	sort = sort !== 'ASC' ? 'DESC' : 'ASC';
	const compare = sort === 'DESC' ? '<' : '>';
	const attributes = {
		suggestedMovies: [
			'uuid',
			'title',
			'overview',
			'poster',
			'backdrop',
			'adult',
			'releasedDate',
			'runtime',
			'ratingCount',
			'ratingAverage',
			'popularity',
			'matchingGenres',
		],
		genres: ['name'],
		movieGenres: ['GenreUuid'],
	};
	const fields = getSelectFields(attributes);
	const sqlQuery = selectSuggestedMovies({
		sort,
		compare,
		moviesLimit,
		matchingGenres,
		uuid,
		userUuid,
		fields,
	});
	try {
		const result = await executeSelectQuery(sqlQuery);
		const movies = getMoviesFromResult(result);
		return getMoviesQuery(movies, moviesLimit);
	} catch (error) {
		throw new StatusError([{ msg: error.message }], 500);
	}
};

const getFavoriteMovies = async (query) => {
	let { sort, limit, uuid, popularity, userUuid } = sanitizeInputs(
		query,
		['sort', 'limit', 'uuid', 'popularity', 'userUuid'],
		['sort', 'limit']
	);
	const moviesLimit = getLimit(limit);
	sort = sort !== 'ASC' ? 'DESC' : 'ASC';
	const compare = sort === 'DESC' ? '<' : '>';
	const attributes = {
		temp: [
			'uuid',
			'title',
			'overview',
			'poster',
			'backdrop',
			'adult',
			'releasedDate',
			'runtime',
			'ratingCount',
			'ratingAverage',
			'popularity',
		],
		genres: ['name'],
		movieGenres: ['GenreUuid'],
	};
	const fields = getSelectFields(attributes);
	const sqlQuery = selectFavoriteMovies({
		sort,
		compare,
		moviesLimit,
		uuid,
		popularity,
		userUuid,
		fields,
	});
	try {
		const result = await executeSelectQuery(sqlQuery);
		const movies = getMoviesFromResult(result);
		return getMoviesQuery(movies, moviesLimit);
	} catch (error) {
		throw new StatusError([{ msg: error.message }], 500);
	}
};

const insertMovie = async (movie) => {
	try {
		const { fields, value } = getObjectValue(movie, FIELDS);
		const query = `INSERT INTO movies (${fields}) VALUES (${value});`;
		await executeInsertQuery(query);
		movie.uuid = sliceUuid(value[0]);
		return movie;
	} catch (error) {
		throw new StatusError([{ msg: error.message }], 500);
	}
};

const insertMovies = async (movies) => {
	try {
		const fields = ['uuid', ...FIELDS];
		const values = [];
		movies.forEach((movie) => {
			const { value } = getObjectValue(movie, FIELDS);
			values.push(`(${value})`);
		});
		const query = `INSERT INTO movies (${fields}) VALUES ${values};`;
		const res = await executeInsertQuery(query);
		return res;
	} catch (error) {
		throw new StatusError([{ msg: error.message }], 500);
	}
};

const deleteMovie = async (uuid) => {
	try {
		uuid = sanitizeInput(uuid);
		const query = `DELETE FROM movies WHERE uuid = ${uuid};`;
		return await executeDeleteQuery(query);
	} catch (error) {
		throw new StatusError([{ msg: error.message }], 500);
	}
};

module.exports = {
	getMovieById,
	getMoviesByName,
	getPopularMovies,
	getTrendingMovies,
	getSuggestedMovies,
	getFavoriteMovies,
	insertMovie,
	insertMovies,
	deleteMovie,
};

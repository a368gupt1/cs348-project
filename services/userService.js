const bcrypt = require('bcryptjs');
const StatusError = require('../errors');

const { generateToken } = require('./jwtService');
const {
	sanitizeInput,
	executeSelectQuery,
	executeUpdateQuery,
	getObjectValue,
	executeInsertQuery,
	sanitizeInputs,
	sliceUuid,
} = require('../repositories/utilRepo');

const FIELDS = ['username', 'email', 'role', 'profile', 'password'];

const getUserById = async (uuid) => {
	try {
		const user = (
			await executeSelectQuery(
				`SELECT * FROM users WHERE uuid = ${sanitizeInput(uuid)};`
			)
		)[0];
		if (!user) {
			throw new Error('User does not exist');
		}
		return user;
	} catch (error) {
		throw new StatusError([{ msg: error.message }], 500);
	}
};

const getAllUsers = async () => {
	try {
		return await executeSelectQuery('SELECT * FROM users;');
	} catch (error) {
		throw new StatusError([{ msg: error.message }], 500);
	}
};

const updateUserProfile = async (uuid, body) => {
	const { profile, username } = sanitizeInputs(body, ['profile', 'username']);
	try {
		const query = `UPDATE users SET profile = ${profile}, username = ${username} WHERE uuid = ${sanitizeInput(
			uuid
		)};`;
		return await executeUpdateQuery(query);
	} catch (error) {
		throw new StatusError([{ msg: error.message }], 500);
	}
};

const login = async (email, password) => {
	try {
		const user = (
			await executeSelectQuery(
				`SELECT * FROM users WHERE email = ${sanitizeInput(email)};`
			)
		)[0];
		if (!user) {
			throw new Error('Invalid credentials');
		}

		const passwordMatches = await bcrypt.compare(password, user.password);
		if (!passwordMatches) {
			throw new Error('Invalid credentials');
		}

		const payload = {
			user: {
				uuid: user.uuid,
			},
		};
		const token = await generateToken(payload);
		return { user, token };
	} catch (error) {
		throw new StatusError([{ msg: error.message }], 500);
	}
};

const register = async ({
	email,
	username,
	password,
	profile,
	role = 'User',
}) => {
	try {
		const emailExists = (
			await executeSelectQuery(
				`SELECT * FROM users WHERE email = ${sanitizeInput(email)};`
			)
		)[0];
		if (!!emailExists) {
			throw new Error('Email already exists');
		}
		let user = {
			email,
			username,
			profile,
			role,
		};
		const salt = await bcrypt.genSalt(10);
		user.password = await bcrypt.hash(password, salt);

		const { fields, value } = getObjectValue(user, FIELDS);
		const addQuery = `INSERT INTO users (${fields}) VALUES (${value});`;
		await executeInsertQuery(addQuery);
		user.uuid = sliceUuid(value[0]);

		const payload = {
			user: {
				uuid: user.uuid,
			},
		};
		const token = await generateToken(payload);
		return { user, token };
	} catch (error) {
		throw new StatusError([{ msg: error.message }], 500);
	}
};

module.exports = {
	getUserById,
	getAllUsers,
	login,
	register,
	updateUserProfile,
};

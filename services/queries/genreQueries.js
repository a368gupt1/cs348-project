const selectGenresStats = () => {
	return `SELECT 
  name, 
  AVG(ratingAverage) as avgRating, 
  MIN(ratingAverage) as minRating, 
  MAX(ratingAverage) as maxRating, 
  COUNT(*) as moviesCount 
FROM 
  genres 
  JOIN movieGenres ON genres.uuid = movieGenres.genreUuid 
  JOIN movies ON movies.uuid = movieGenres.movieUuid 
GROUP BY 
  genres.uuid 
ORDER BY 
  name ASC;
`;
};

module.exports = { selectGenresStats };

const selectMovieWithId = (uuid) => `WITH temp AS (
  SELECT 
    *, 
    RANK() OVER(
      ORDER BY 
        popularity DESC
    ) AS popularityRanking 
  FROM 
    movies
  ) 
SELECT 
  * 
FROM 
  temp 
WHERE 
  uuid = ${uuid};
`;

const selectPopularMovies = ({
	sort,
	compare,
	moviesLimit,
	popularity,
	uuid,
	fields,
}) => {
	const where =
		popularity && uuid
			? `WHERE popularity ${compare} ${popularity} OR (popularity = ${popularity} AND uuid ${compare} ${uuid})`
			: '';
	return `WITH temp AS (
    SELECT 
      * 
    FROM 
      movies ${where} 
    ORDER BY 
      popularity ${sort}, 
      uuid ${sort} 
    LIMIT 
      ${moviesLimit}
  ) 
  SELECT 
    ${fields} 
  FROM 
    temp 
    LEFT JOIN movieGenres ON movieGenres.movieUuid = temp.uuid 
    LEFT JOIN genres ON movieGenres.GenreUuid = genres.uuid 
  ORDER BY 
    popularity ${sort}, 
    uuid ${sort};
  `;
};

const selectTrendingMovies = ({
	sort,
	compare,
	moviesLimit,
	ratingAverage,
	uuid,
	fields,
}) => {
	const where =
		ratingAverage && uuid
			? `WHERE ratingAverage ${compare} ${ratingAverage} OR (ratingAverage = ${ratingAverage} AND uuid ${compare} ${uuid})`
			: '';
	return `WITH temp AS (
    SELECT 
      * 
    FROM 
      movies ${where} 
    ORDER BY 
      ratingAverage ${sort}, 
      uuid ${sort} 
    LIMIT 
      ${moviesLimit}
  ) 
  SELECT 
    ${fields} 
  FROM 
    temp 
    LEFT JOIN movieGenres ON movieGenres.movieUuid = temp.uuid 
    LEFT JOIN genres ON movieGenres.GenreUuid = genres.uuid 
  ORDER BY 
    ratingAverage ${sort}, 
    uuid ${sort};
  `;
};

const selectFavoriteMovies = ({
	sort,
	compare,
	moviesLimit,
	uuid,
  popularity,
	userUuid,
	fields,
}) => {
	const where =
		popularity && uuid
			? `WHERE popularity ${compare} ${popularity} OR (popularity = ${popularity} AND uuid ${compare} ${uuid})`
			: '';
	return `WITH favouriteMovies AS (
    SELECT 
      movies.uuid, 
      movies.title, 
      movies.overview, 
      movies.poster, 
      movies.backdrop, 
      movies.adult, 
      movies.releasedDate, 
      movies.runtime, 
      movies.ratingCount, 
      movies.ratingAverage, 
      movies.popularity 
    FROM 
      movies 
      JOIN favourites ON movies.uuid = favourites.movieUuid 
      AND favourites.userUuid = ${userUuid}
  ), 
  temp AS (
    SELECT 
      * 
    FROM 
      favouriteMovies ${where} 
    ORDER BY 
      popularity ${sort}, 
      uuid ${sort} 
    LIMIT 
      ${moviesLimit}
  ) 
  SELECT 
    ${fields} 
  FROM 
    temp 
    LEFT JOIN movieGenres ON movieGenres.movieUuid = temp.uuid 
    LEFT JOIN genres ON movieGenres.GenreUuid = genres.uuid 
  ORDER BY 
    popularity ${sort}, 
    uuid ${sort};
  `
  ;
};

const selectSuggestedMovies = ({
	sort,
	compare,
	moviesLimit,
	matchingGenres,
	uuid,
	userUuid,
	fields,
}) => {
	const having =
		matchingGenres && uuid
			? `HAVING matchingGenres ${compare} ${matchingGenres} OR (matchingGenres = ${matchingGenres} AND uuid ${compare} ${uuid})`
			: '';
	return `WITH userFavouriteGenres AS (
    SELECT 
      genreUuid 
    FROM 
      favourites 
      JOIN movieGenres ON favourites.movieUuid = movieGenres.movieUuid 
    WHERE 
      favourites.userUuid = ${userUuid}
  ), 
  suggestedMovies AS (
    SELECT 
      movies.*, 
      COUNT(DISTINCT movieGenres.genreUuid) AS matchingGenres 
    FROM 
      movies 
      JOIN movieGenres ON movies.uuid = movieGenres.movieUuid 
    WHERE 
      movies.uuid NOT IN (
        SELECT 
          movieUuid 
        FROM 
          favourites 
        WHERE 
          userUuid = ${userUuid}
      ) 
      AND movieGenres.genreUuid IN (
        SELECT 
          genreUuid 
        FROM 
          userFavouriteGenres
      ) 
    GROUP BY 
      movies.uuid ${having} 
    ORDER BY 
      matchingGenres ${sort}, 
      uuid ${sort} 
    LIMIT 
      ${moviesLimit}
  ) 
  SELECT 
    ${fields} 
  FROM 
    suggestedMovies 
    JOIN movieGenres ON suggestedMovies.uuid = movieGenres.movieUuid 
    JOIN genres ON genres.uuid = movieGenres.genreUuid 
  ORDER BY 
    matchingGenres ${sort}, 
    uuid ${sort};
  `;
};

module.exports = {
	selectMovieWithId,
	selectPopularMovies,
	selectTrendingMovies,
	selectSuggestedMovies,
	selectFavoriteMovies,
};

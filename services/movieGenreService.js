const StatusError = require('../errors');
const {
	sanitizeInput,
	executeInsertQuery,
} = require('../repositories/utilRepo');

const insertMovieGenre = async (movieGenre) => {
	try {
		const genreUuid = sanitizeInput(movieGenre.genreUuid);
		const movieUuid = sanitizeInput(movieGenre.movieUuid);
		const query = `INSERT INTO movieGenres (movieUuid, genreUuid) VALUES (${movieUuid}, ${genreUuid});`;
		return await executeInsertQuery(query);
	} catch (error) {
		throw new StatusError([{ msg: error.message }], 500);
	}
};

module.exports = {
	insertMovieGenre,
};

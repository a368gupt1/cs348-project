const StatusError = require('../errors');
const {
	executeSelectQuery,
	sanitizeInput,
	getObjectValue,
	executeInsertQuery,
	executeUpdateQuery,
	sliceUuid,
} = require('../repositories/utilRepo');

const FIELDS = ['content', 'rating', 'userUuid', 'movieUuid'];

const round = (value, decimals) => {
	return parseFloat(value.toFixed(decimals));
};

const insertReview = async (review) => {
	try {
		const reviewed = await getReview(review.userUuid, review.movieUuid);
		if (!!reviewed) {
			throw new Error('Already reviewed movie');
		}
		const { fields, value } = getObjectValue(review, FIELDS);

		await executeInsertQuery(
			`INSERT INTO reviews (${fields}) VALUES (${value});`
		);
		review.uuid = sliceUuid(value[0]);

		// update movie rating
		// const sanitizedMovieId = sanitizeInput(review.movieUuid);
		// let { ratingCount, ratingAverage } = (
		// 	await executeSelectQuery(
		// 		`SELECT ratingCount, ratingAverage FROM movies WHERE uuid=${sanitizedMovieId};`
		// 	)
		// )[0];
		// const ratingTotal = ratingAverage * ratingCount + review.rating;
		// ratingCount++;
		// ratingAverage = round(ratingTotal / ratingCount, 4);
		// await executeUpdateQuery(
		// 	`UPDATE movies SET ratingCount=${ratingCount}, ratingAverage=${ratingAverage} WHERE uuid=${sanitizedMovieId};`
		// );

		return review;
	} catch (error) {
		throw new StatusError([{ msg: error.message }], 500);
	}
};

const getReview = async (userUuid, movieUuid) => {
	try {
		userUuid = sanitizeInput(userUuid);
		movieUuid = sanitizeInput(movieUuid);
		return (
			(
				await executeSelectQuery(
					`SELECT * FROM reviews WHERE userUuid=${userUuid} AND movieUuid=${movieUuid};`
				)
			)[0] || null
		);
	} catch (error) {
		throw new StatusError([{ msg: error.message }], 500);
	}
};

const getReviews = async (movieUuid) => {
	try {
		movieUuid = sanitizeInput(movieUuid);
		return await executeSelectQuery(
			`SELECT reviews.uuid AS uuid, content, userUuid, username, role, movieUuid, rating FROM reviews JOIN users on reviews.userUuid=users.uuid AND movieUuid=${movieUuid};`
		);
	} catch (error) {
		throw new StatusError([{ msg: error.message }], 500);
	}
};

module.exports = {
	insertReview,
	getReview,
	getReviews,
};

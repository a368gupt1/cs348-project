const { QueryTypes } = require('sequelize');
const { sequelize } = require('../models');
const { v4: uuidv4 } = require('uuid');

const executeQuery = (query, option) => {
	return sequelize.query(query, option);
};

const executeSelectQuery = (query) => {
	return sequelize.query(query, {
		type: QueryTypes.SELECT,
	});
};

const executeInsertQuery = (query) => {
	return sequelize.query(query, {
		type: QueryTypes.INSERT,
	});
};

const executeUpdateQuery = (query) => {
	return sequelize.query(query, {
		type: QueryTypes.UPDATE,
	});
};

const executeDeleteQuery = (query) => {
	return sequelize.query(query, {
		type: QueryTypes.DELETE,
	});
};

const sanitizeInput = (input) => {
	return sequelize.escape(input);
};

const sanitizeInputs = (query, fields, ignoreList = []) => {
	const res = {};
	fields.forEach((field) => {
		if (query[field]) {
			if (ignoreList.includes(field)) {
				res[field] = query[field];
			} else {
				res[field] = sanitizeInput(query[field]);
			}
		}
	});
	return res;
};

const getSelectFields = (attributes) => {
	const fields = [];
	for (const [key, values] of Object.entries(attributes)) {
		for (const value of values) {
			fields.push(`${key}.${value}`);
		}
	}
	return fields;
};

const getObjectValue = (obj, attributes) => {
	const value = [];
	const fields = [...attributes];
	fields.forEach((field) => {
		if (obj[field] === null) {
			value.push('NULL');
		} else if (obj[field] === false) {
			value.push('0');
		} else if (obj[field] === true) {
			value.push('1');
		} else {
			value.push(sanitizeInput(obj[field]));
		}
	});
	fields.unshift('uuid');
	value.unshift(sanitizeInput(uuidv4()));
	return {
		fields,
		value,
	};
};

const sliceUuid = (uuid) => {
	return uuid.slice(1, uuid.length - 1);
};

module.exports = {
	executeQuery,
	executeSelectQuery,
	executeInsertQuery,
	executeUpdateQuery,
	executeDeleteQuery,
	sanitizeInput,
	sanitizeInputs,
	getSelectFields,
	getObjectValue,
	sliceUuid,
};

const express = require('express');
const router = express.Router();
const favouriteService = require('../services/favouriteService');

const { defaultErrorHandler, defaultRequestValidator } = require('./utils');

const auth = require('../middleware/auth');

router.get('/favourite/:movieId', auth, async (req, res) => {
  // #swagger.tags = ['favourite']
	try {
		const hasFavourited = await favouriteService.hasFavourited({
			userUuid: req.user.uuid,
			movieUuid: req.params.movieId,
		});
		return res.json(hasFavourited);
	} catch (error) {
		defaultErrorHandler(res, error);
	}
});

// Add movie to favourite
router.post('/favourite/:movieId', auth, async (req, res) => {
	// #swagger.tags = ['favourite']
	try {
		defaultRequestValidator(req);
		const { uuid } = req.user;
		const favourite = await favouriteService.insertFavourite({
			movieUuid: req.params.movieId,
			userUuid: uuid,
		});
		/* #swagger.responses[200] = {
					description: 'Success',
					schema: { $ref: '#/definitions/User' }
		} */
		return res.json(favourite);
	} catch (error) {
		/* #swagger.responses[401] = {
					description: 'Unauthorized',
					schema: { $ref: '#/definitions/ErrorResponse' }
		} */
		/* #swagger.responses[404] = {
					description: 'Not found',
					schema: { $ref: '#/definitions/ErrorResponse' }
		} */
		return defaultErrorHandler(res, error);
	}
});

// Remove movie to favourite
router.delete('/favourite/:movieId', auth, async (req, res) => {
	// #swagger.tags = ['favourite']
	try {
		defaultRequestValidator(req);
		const { uuid } = req.user;
		await favouriteService.deleteFavourite({
			movieUuid: req.params.movieId,
			userUuid: uuid,
		});
		/* #swagger.responses[200] = {
					description: 'Success',
					schema: { $ref: '#/definitions/User' }
		} */
		return res.json('Removed succesfully');
	} catch (error) {
		/* #swagger.responses[401] = {
					description: 'Unauthorized',
					schema: { $ref: '#/definitions/ErrorResponse' }
		} */
		/* #swagger.responses[404] = {
					description: 'Not found',
					schema: { $ref: '#/definitions/ErrorResponse' }
		} */
		return defaultErrorHandler(res, error);
	}
});

module.exports = router;

const express = require('express');
const router = express.Router();
const movieService = require('../services/movieService');
const genreService = require('../services/genreService');
const { defaultErrorHandler } = require('./utils');

const auth = require('../middleware/auth');

/**
 * Get popular movies
 * @param sort either 'ASC' or 'DESC'
 * @param limit limit
 * @param popularity popularity cursor
 * @param uuid uuid cursor
 */
router.get('/popular', async (req, res) => {
	try {
		// #swagger.tags = ['movie']
		// #swagger.tags = ['movie']
		// #swagger.tags = ['movie']
		/* #swagger.responses[200] = {
            description: 'Success',
            schema: { $ref: '#/definitions/MovieResponse' }
          } */
		/* #swagger.responses[400] = {
						description: 'Bad request',
						schema: { $ref: '#/definitions/ErrorResponse' }
			} */
		/* #swagger.responses[401] = {
						description: 'Unauthorized',
						schema: { $ref: '#/definitions/ErrorResponse' }
			} */
		/* #swagger.responses[404] = {
						description: 'Not found',
						schema: { $ref: '#/definitions/ErrorResponse' }
			} */
		const result = await movieService.getPopularMovies(req.query);
		return res.json(result);
	} catch (error) {
		return defaultErrorHandler(res, error);
	}
});

/**
 * Get trending movies
 * @param sort either 'ASC' or 'DESC'
 * @param limit limit
 * @param ratingAverage ratingAverage cursor
 * @param uuid uuid cursor
 */
router.get('/trending', async (req, res) => {
	try {
		// #swagger.tags = ['movie']
		// #swagger.tags = ['movie']
		// #swagger.tags = ['movie']
		/* #swagger.responses[200] = {
            description: 'Success',
            schema: { $ref: '#/definitions/MovieResponse' }
          } */
		/* #swagger.responses[400] = {
						description: 'Bad request',
						schema: { $ref: '#/definitions/ErrorResponse' }
			} */
		/* #swagger.responses[401] = {
						description: 'Unauthorized',
						schema: { $ref: '#/definitions/ErrorResponse' }
			} */
		/* #swagger.responses[404] = {
						description: 'Not found',
						schema: { $ref: '#/definitions/ErrorResponse' }
			} */
		const result = await movieService.getTrendingMovies(req.query);
		return res.json(result);
	} catch (error) {
		return defaultErrorHandler(res, error);
	}
});

/**
 * Get trending movies
 * @param sort either 'ASC' or 'DESC'
 * @param limit limit
 * @param ratingAverage ratingAverage cursor
 * @param uuid uuid cursor
 */
router.get('/suggested', auth, async (req, res) => {
	try {
		// #swagger.tags = ['movie']
		// #swagger.tags = ['movie']
		// #swagger.tags = ['movie']
		/* #swagger.responses[200] = {
            description: 'Success',
            schema: { $ref: '#/definitions/MovieResponse' }
          } */
		/* #swagger.responses[400] = {
						description: 'Bad request',
						schema: { $ref: '#/definitions/ErrorResponse' }
			} */
		/* #swagger.responses[401] = {
						description: 'Unauthorized',
						schema: { $ref: '#/definitions/ErrorResponse' }
			} */
		/* #swagger.responses[404] = {
						description: 'Not found',
						schema: { $ref: '#/definitions/ErrorResponse' }
			} */
		const result = await movieService.getSuggestedMovies({
			...req.query,
			userUuid: req.user.uuid,
		});
		return res.json(result);
	} catch (error) {
		return defaultErrorHandler(res, error);
	}
});

/**
 * Get favourite movies
 * @param sort either 'ASC' or 'DESC'
 * @param limit limit
 * @param popularity popularity cursor
 * @param uuid uuid cursor
 */
router.get('/favourites', auth, async (req, res) => {
	try {
		// #swagger.tags = ['movie']
		// #swagger.tags = ['movie']
		/* #swagger.responses[200] = {
            description: 'Success',
            schema: { $ref: '#/definitions/MovieResponse' }
          } */
		/* #swagger.responses[400] = {
						description: 'Bad request',
						schema: { $ref: '#/definitions/ErrorResponse' }
			} */
		/* #swagger.responses[401] = {
						description: 'Unauthorized',
						schema: { $ref: '#/definitions/ErrorResponse' }
			} */
		/* #swagger.responses[404] = {
						description: 'Not found',
						schema: { $ref: '#/definitions/ErrorResponse' }
			} */
		const result = await movieService.getFavoriteMovies({
			...req.query,
			userUuid: req.user.uuid,
		});
		return res.json(result);
	} catch (error) {
		return defaultErrorHandler(res, error);
	}
});

// Get movie by name
router.get('/movieName', async (req, res) => {
	try {
		// #swagger.tags = ['movie']
		/* #swagger.responses[200] = {
            description: 'Success',
            schema: { $ref: '#/definitions/MovieNameResponse' }
          } */
		/* #swagger.responses[400] = {
						description: 'Bad request',
						schema: { $ref: '#/definitions/ErrorResponse' }
			} */
		/* #swagger.responses[401] = {
						description: 'Unauthorized',
						schema: { $ref: '#/definitions/ErrorResponse' }
			} */
		/* #swagger.responses[404] = {
						description: 'Not found',
						schema: { $ref: '#/definitions/ErrorResponse' }
			} */
		const movies = await movieService.getMoviesByName(req.query.title);
		return res.json(movies);
	} catch (error) {
		return defaultErrorHandler(res, error);
	}
});

// Get movies stats for each genre
router.get('/genres', async (req, res) => {
	try {
		// #swagger.tags = ['movie']
		/* #swagger.responses[200] = {
            description: 'Success',
            schema: { $ref: '#/definitions/StatisticsResponse' }
          } */
		const response = await genreService.getAllGenres();
		return res.json(response);
	} catch (error) {
		return defaultErrorHandler(res, error);
	}
});

// Get movie by id
router.get('/movie/:movieId', async (req, res) => {
	try {
		// #swagger.tags = ['movie']
		const movie = await movieService.getMovieById(req.params.movieId);
		return res.json(movie);
	} catch (error) {
		return defaultErrorHandler(res, error);
	}
});

module.exports = router;

# CS348 Group Project
Start by cloning the repository by running `git clone https://git.uwaterloo.ca/a368gupt1/cs348-project.git cs348`.

Make sure you have [Node.js](https://nodejs.org/en/download/) and [MySQL](https://dev.mysql.com/doc/refman/8.0/en/installing.html) installed.

## To seed sample data:
1. Run `cd cs348`.
2. Run `npm run install-server`.
3. Create a file named `.env` in the source directory. Then paste the following lines into that file, replacing username, password, and database with your database credentials.
```
MYSQL_USERNAME="username"
MYSQL_PASSWORD="password"
MYSQL_DATABASE="database"
```
4. Run `npm run data:dev`.

## To seed production data:
1. Do steps 1-3 in the previous section
2. Run `npm run data:prod`.

## To run the app:
1. Run `cd cs348`.
2. Run `npm run install-server`.
3. Run `npm run install-client`.
4. Run `npm run dev`.

## To check API documentation:
1. Run `npm run swagger`.
2. Run `npm run server`.
3. Open http://localhost:8000/api/docs.

## Features:
- Create an account and personalize your profile with encrypted password protection.
- Explore the latest movies, sort them by popularity or rating, and add or remove them from your favorites list.
- Get the inside scoop on each genre with in-depth statistics and average ratings.
- Discover new movies tailored to your tastes with personalized recommendations based on your favorite films.
- Share your thoughts and opinions by leaving detailed movie reviews.
- Seamlessly browse through endless movie options with infinite loading and pagination.
- Quickly find your favorite movies with a search feature that lets you search by name.
- Switch between sleek dark mode or light mode themes.
- Easily access and navigate the app's API documentation with the help of Swagger.
- Implement security measures that safeguard against SQL injection attacks.
- Use indexes to optimize data retrieval.

## Notes:
- If you seed data using the npm scripts, then the ids will be different than those used in the test-sample.sql and test-production.sql files. However, there would be no change to how the app works.
- If you want to use the exact data set that was used for the test-sample.sql and test-production.sql files, you can copy the INSERT queries in all of the files inside the /sample and /production directories, respectively, and then run them using MySQL Workbench.

const swaggerAutogen = require('swagger-autogen')({ openapi: '3.0.0' });

const outputFile = './swagger.json';
const endpointsFiles = ['./server.js'];

const doc = {
	info: {
		title: 'CS348 Group Project',
	},
	host: 'localhost:8000',
	tags: [
		{
			name: 'user',
			description: 'User endpoints',
		},
		{
			name: 'movie',
			description: 'Movie endpoints',
		},
    {
			name: 'favourite',
			description: 'Favourite endpoints',
		},
    {
			name: 'review',
			description: 'Review endpoints',
		},
		{
			name: 'genre',
			description: 'Genre endpoints',
		},
	],
	definitions: {
		LoginParams: {
			$email: 'johndoe@example.com',
			$password: '123456',
		},
		RegisterParams: {
			$email: 'johndoe@example.com',
			$username: 'johndoe',
			$password: '123456',
			$profile: 'Hello world',
		},
		AuthResponse: {
			user: {
				$uuid: '1812a952-513d-4474-94d0-ef8a6c092fdb',
				$email: 'johndoe@example.com',
				$password: '123456',
				$profile: 'Hello world!',
				$role: 'Admin',
			},
			token:
				'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InV1aWQiOiJmYmZkNDNhYS1kYjQzLTRhODctYmE2Ni1jYjM1MjYyMjliMDIifSwiaWF0IjoxNjc1NjYwMjM5LCJleHAiOjE2NzU2NjM4Mzl9._BReuhfRbxNmCnRmjh8B660rBUeiPK5XddNk5iQkFKA',
		},
		ErrorResponse: {
			errors: [
				{
					msg: 'Some error message',
				},
			],
		},
		MovieNameResponse: [
			{
				$value: '21eb15c9-c54c-4b46-8b2b-dac91f9a0015',
				$label: 'Ratatouille',
			},
		],
		MovieResponse: {
			$movies: [
				{
					$uuid: '1812a952-513d-4474-94d0-ef8a6c092fdb',
					$title: 'Ratatouille',
					$overview: 'Did I spell that right?...',
					$poster:
						'http://image.tmdb.org/t/p/original/kuf6dutpsT0vSVehic3EZIqkOBt.jpg',
					$backdrop:
						'http://image.tmdb.org/t/p/original/kuf6dutpsT0vSVehic3EZIqkOBt.jpg',
					$adult: false,
					$releasedDate: '2022-12-31',
					$runtime: 161,
					$ratingCount: 1200,
					$ratingAverage: 7.577,
					$popularity: 5462,
					$genres: [
						{
							uuid: '3eec78cd-4a7b-4a35-902f-d58450acf6c5',
							name: 'Drama',
						},
					],
				},
			],
			$hasMore: true,
		},
		StatisticsResponse: {
			$labels: [
				'Action',
				'Adventure',
				'Animation',
				'Comedy',
				'Crime',
				'Documentary',
				'Drama',
				'Family',
				'Fantasy',
				'History',
				'Horror',
				'Music',
				'Mystery',
				'Romance',
				'Science Fiction',
				'Thriller',
				'TV Movie',
				'War',
				'Western',
			],
			$moviesCount: [
				726, 536, 384, 509, 230, 23, 575, 342, 396, 50, 331, 31, 161, 215, 334,
				537, 30, 45, 17,
			],
			$avgRating: [
				'6.55983499',
				'6.72490485',
				'7.09465104',
				'6.63675246',
				'6.65273478',
				'7.00869565',
				'6.76628035',
				'6.94636257',
				'6.70623485',
				'7.22560000',
				'6.23672810',
				'7.29448387',
				'6.61901863',
				'6.21406512',
				'6.56104192',
				'6.45530764',
				'6.58933333',
				'7.35346667',
				'6.87941176',
			],
			$minRating: [
				'0.0000',
				'0.0000',
				'0.0000',
				'0.0000',
				'0.0000',
				'5.3670',
				'0.0000',
				'0.0000',
				'0.0000',
				'0.0000',
				'0.0000',
				'5.6420',
				'0.0000',
				'0.0000',
				'0.0000',
				'0.0000',
				'0.0000',
				'5.1000',
				'4.9150',
			],
			$maxRating: [
				'9.8692',
				'8.6540',
				'8.6540',
				'8.7710',
				'8.7150',
				'8.3150',
				'9.8692',
				'8.6540',
				'8.7710',
				'8.5660',
				'8.5000',
				'8.3760',
				'8.5000',
				'8.5790',
				'8.4070',
				'9.8692',
				'8.2210',
				'8.7860',
				'8.4830',
			],
		},
		User: {
			$uuid: '1812a952-513d-4474-94d0-ef8a6c092fdb',
			$email: 'johndoe@example.com',
			$password: '123456',
			$profile: 'Hello world!',
			$role: 'Admin',
		},
		Movie: {
			$uuid: '1812a952-513d-4474-94d0-ef8a6c092fdb',
			$title: 'Ratatouille',
			$overview: 'Did I spell that right?...',
			$poster:
				'http://image.tmdb.org/t/p/original/kuf6dutpsT0vSVehic3EZIqkOBt.jpg',
			$backdrop:
				'http://image.tmdb.org/t/p/original/kuf6dutpsT0vSVehic3EZIqkOBt.jpg',
			$adult: false,
			$releasedDate: '2022-12-31',
			$runtime: 161,
			$ratingCount: 1200,
			$ratingAverage: 7.577,
			$popularity: 5462,
		},
		Genre: {
			$uuid: '1812a952-513d-4474-94d0-ef8a6c092fdb',
			$name: 'Action',
		},
	},
};

swaggerAutogen(outputFile, endpointsFiles, doc);

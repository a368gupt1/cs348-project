import { createSlice, PayloadAction } from '@reduxjs/toolkit';

interface Auth {
	uuid: string;
	username: string;
	email: string;
	role: 'User' | 'Admin' | '';
	profile: string;
	isAuthenticated: boolean;
	loading: boolean;
	token: string;
}

const initialState: Auth = {
	uuid: '',
	username: '',
	email: '',
	role: '',
	profile: '',
	isAuthenticated: false,
	loading: false,
	token: '',
};

const authSlice = createSlice({
	name: 'auth',
	initialState,
	reducers: {
		saveUser: (state, { payload }: PayloadAction<Auth>) => {
			state.uuid = payload.uuid;
			state.username = payload.username;
			state.email = payload.email;
			state.role = payload.role;
			state.profile = payload.profile;
			state.isAuthenticated = true;
			state.token = payload.token;
		},
		clearUser: (state) => {
			state.uuid = '';
			state.username = '';
			state.email = '';
			state.role = '';
			state.profile = '';
			state.isAuthenticated = false;
			state.token = '';
		},
		setLoading(state, { payload }: PayloadAction<boolean>) {
			state.loading = payload;
		},
		saveProfile(
			state,
			{
				payload: { username, profile },
			}: PayloadAction<{ username: string; profile: string }>
		) {
			state.username = username;
			state.profile = profile;
		},
	},
});

export const { saveUser, clearUser, setLoading, saveProfile } =
	authSlice.actions;

export default authSlice.reducer;

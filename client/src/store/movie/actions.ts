import { AppDispatch } from '../index';
import axios from 'axios';
import { MovieListTitles, SortBy } from '../../types';

import { addMovies, initMovies, setLoading } from './movieSlice';
import { showNotification } from '../ui/actions';

const url = 'http://localhost:8000';

export const getPopularMovies = ({ sort }: { sort: SortBy }) => {
	return async (dispatch: AppDispatch) => {
		dispatch(setLoading({ page: MovieListTitles.POPULAR, loading: true }));
		dispatch(
			showNotification({
				status: 'default',
				msg: 'Fetching movies...',
			})
		);
		try {
			const response = await axios.get(`${url}/api/movies/popular`, {
				params: { sort },
			});
			const movieData = response.data;
			const fieldCursor = movieData.movies.at(-1).popularity;
			const uuidCursor = movieData.movies.at(-1).uuid;
			dispatch(
				initMovies({
					page: MovieListTitles.POPULAR,
					sort,
					movieData,
					fieldCursor,
					uuidCursor,
				})
			);
			dispatch(
				showNotification({
					status: 'success',
					msg: 'Fetched movies 🎉',
				})
			);
		} catch (error: any) {
			dispatch(
				showNotification({
					status: 'error',
					msg: error.message ?? 'Failed to fetch movies 😢',
				})
			);
		}
		dispatch(setLoading({ page: MovieListTitles.POPULAR, loading: false }));
	};
};

export const fetchMorePopularMovies = ({
	sort,
	popularity,
	uuid,
}: {
	sort: SortBy;
	popularity: number;
	uuid: string;
}) => {
	return async (dispatch: AppDispatch) => {
		dispatch(
			showNotification({
				status: 'default',
				msg: 'Fetching more movies...',
			})
		);
		try {
			const response = await axios.get(`${url}/api/movies/popular`, {
				params: { sort, popularity, uuid },
			});
			const movieData = response.data;
			const fieldCursor = movieData.movies.at(-1).popularity;
			const uuidCursor = movieData.movies.at(-1).uuid;
			dispatch(
				addMovies({
					page: MovieListTitles.POPULAR,
					movieData,
					fieldCursor,
					uuidCursor,
				})
			);
			dispatch(
				showNotification({
					status: 'success',
					msg: 'Fetched more movies 🎉',
				})
			);
		} catch (error: any) {
			dispatch(
				showNotification({
					status: 'error',
					msg: error.message ?? 'Failed to fetch movies 😢',
				})
			);
		}
		dispatch(setLoading({ page: MovieListTitles.POPULAR, loading: false }));
	};
};

export const getTrendingMovies = ({ sort }: { sort: SortBy }) => {
	return async (dispatch: AppDispatch) => {
		dispatch(setLoading({ page: MovieListTitles.TRENDING, loading: true }));
		dispatch(
			showNotification({
				status: 'default',
				msg: 'Fetching movies...',
			})
		);
		try {
			const response = await axios.get(`${url}/api/movies/trending`, {
				params: { sort },
			});
			const movieData = response.data;
			const fieldCursor = movieData.movies.at(-1).ratingAverage;
			const uuidCursor = movieData.movies.at(-1).uuid;
			dispatch(
				initMovies({
					page: MovieListTitles.TRENDING,
					sort,
					movieData,
					fieldCursor,
					uuidCursor,
				})
			);
			dispatch(
				showNotification({
					status: 'success',
					msg: 'Fetched movies 🎉',
				})
			);
		} catch (error: any) {
			dispatch(
				showNotification({
					status: 'error',
					msg: error.message ?? 'Failed to fetch movies 😢',
				})
			);
		}
		dispatch(setLoading({ page: MovieListTitles.TRENDING, loading: false }));
	};
};

export const fetchMoreTrendingMovies = ({
	sort,
	ratingAverage,
	uuid,
}: {
	sort: SortBy;
	ratingAverage: number;
	uuid: string;
}) => {
	return async (dispatch: AppDispatch) => {
		dispatch(
			showNotification({
				status: 'default',
				msg: 'Fetching more movies...',
			})
		);
		try {
			const response = await axios.get(`${url}/api/movies/trending`, {
				params: { sort, ratingAverage, uuid },
			});
			const movieData = response.data;
			const fieldCursor = movieData.movies.at(-1).ratingAverage;
			const uuidCursor = movieData.movies.at(-1).uuid;
			dispatch(
				addMovies({
					page: MovieListTitles.TRENDING,
					movieData,
					fieldCursor,
					uuidCursor,
				})
			);
			dispatch(
				showNotification({
					status: 'success',
					msg: 'Fetched more movies 🎉',
				})
			);
		} catch (error: any) {
			dispatch(
				showNotification({
					status: 'error',
					msg: error.message ?? 'Failed to fetch movies 😢',
				})
			);
		}
		dispatch(setLoading({ page: MovieListTitles.TRENDING, loading: false }));
	};
};

export const getSuggestedMovies = ({
	token,
	sort,
}: {
	token: string;
	sort: SortBy;
}) => {
	return async (dispatch: AppDispatch) => {
		dispatch(setLoading({ page: MovieListTitles.EXPLORE, loading: true }));
		dispatch(
			showNotification({
				status: 'default',
				msg: 'Fetching movies...',
			})
		);
		try {
			const response = await axios.get(`${url}/api/movies/suggested`, {
				params: { sort },
				headers: {
					'x-auth-token': token,
				},
			});
			const movieData = response.data;
			const fieldCursor = movieData.movies.at(-1)?.matchingGenres ?? 0;
			const uuidCursor = movieData.movies.at(-1)?.uuid ?? '0';
			dispatch(
				initMovies({
					page: MovieListTitles.EXPLORE,
					sort,
					movieData,
					fieldCursor,
					uuidCursor,
				})
			);
			dispatch(
				showNotification({
					status: 'success',
					msg: 'Fetched movies 🎉',
				})
			);
		} catch (error: any) {
			dispatch(
				showNotification({
					status: 'error',
					msg: error.message ?? 'Failed to fetch movies 😢',
				})
			);
		}
		dispatch(setLoading({ page: MovieListTitles.EXPLORE, loading: false }));
	};
};

export const getFavoriteMovies = ({
	token,
	sort,
}: {
	token: string;
	sort: SortBy;
}) => {
	return async (dispatch: AppDispatch) => {
		dispatch(setLoading({ page: MovieListTitles.FAVOURITES, loading: true }));
		dispatch(
			showNotification({
				status: 'default',
				msg: 'Fetching movies...',
			})
		);
		try {
			const response = await axios.get(`${url}/api/movies/favourites`, {
				params: { sort },
				headers: {
					'x-auth-token': token,
				},
			});
			const movieData = response.data;
			const fieldCursor = movieData.movies.at(-1)?.popularity ?? 0;
			const uuidCursor = movieData.movies.at(-1)?.uuid ?? '0';
			dispatch(
				initMovies({
					page: MovieListTitles.FAVOURITES,
					sort,
					movieData,
					fieldCursor,
					uuidCursor,
				})
			);
			dispatch(
				showNotification({
					status: 'success',
					msg: 'Fetched movies 🎉',
				})
			);
		} catch (error: any) {
			dispatch(
				showNotification({
					status: 'error',
					msg: error.message ?? 'Failed to fetch movies 😢',
				})
			);
		}
		dispatch(setLoading({ page: MovieListTitles.FAVOURITES, loading: false }));
	};
};

export const fetchMoreSuggestedMovies = ({
	token,
	sort,
	matchingGenres,
	uuid,
}: {
	token: string;
	sort: SortBy;
	matchingGenres: number;
	uuid: string;
}) => {
	return async (dispatch: AppDispatch) => {
		dispatch(
			showNotification({
				status: 'default',
				msg: 'Fetching more movies...',
			})
		);
		try {
			const response = await axios.get(`${url}/api/movies/suggested`, {
				params: { sort, matchingGenres, uuid },
				headers: {
					'x-auth-token': token,
				},
			});
			const movieData = response.data;
			const fieldCursor = movieData.movies.at(-1).matchingGenres;
			const uuidCursor = movieData.movies.at(-1).uuid;
			dispatch(
				addMovies({
					page: MovieListTitles.EXPLORE,
					movieData,
					fieldCursor,
					uuidCursor,
				})
			);
			dispatch(
				showNotification({
					status: 'success',
					msg: 'Fetched more movies 🎉',
				})
			);
		} catch (error: any) {
			dispatch(
				showNotification({
					status: 'error',
					msg: error.message ?? 'Failed to fetch movies 😢',
				})
			);
		}
		dispatch(setLoading({ page: MovieListTitles.EXPLORE, loading: false }));
	};
};

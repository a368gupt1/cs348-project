import { SimpleGrid } from '@chakra-ui/react';
import axios from 'axios';
import { useEffect, useState } from 'react';
import { useAppSelector } from '../../hooks/useRedux';
import ReviewItem from './ReviewItem';

interface ReviewProps {
	movieId: string;
}

interface Review {
	content: string;
	movieUuid: string;
	rating: number;
	userUuid: string;
	username: string;
	role: string;
	uuid: string;
}

const ReviewList = ({ movieId }: ReviewProps) => {
	const { uuid: userId } = useAppSelector((state) => state.auth);
	const [reviews, setReviews] = useState<Review[]>([]);

	useEffect(() => {
		const fetchReviews = async () => {
			const { data } = await axios.get(
				`http://localhost:8000/api/reviews/reviews/${movieId}`
			);
			setReviews(data.filter((review: Review) => review.userUuid !== userId));
		};
		try {
			fetchReviews();
		} catch (error) {
			console.error(error);
		}
	}, [movieId, userId]);

	return (
		<SimpleGrid my={10} columns={{ base: 1, xl: 2 }} spacing={'20'} mx={'auto'}>
			{reviews.map((reviewInfo, index) => (
				<ReviewItem key={reviewInfo.uuid} {...reviewInfo} index={index} />
			))}
		</SimpleGrid>
	);
};

export default ReviewList;

import { Box, useDisclosure } from '@chakra-ui/react';
import axios from 'axios';
import { useEffect, useState } from 'react';
import Select, { SingleValue } from 'react-select';
import Movie from '../menu/Movie';
import CustomModal from '../modal/CustomModal';

interface Option {
	label: string;
	value: string;
}

const SearchBar = () => {
	const { isOpen, onOpen, onClose } = useDisclosure();
	const [options, setOptions] = useState<Option[]>([]);
	const [title, setTitle] = useState('');
	const [loading, setLoading] = useState(false);
	const [selected, setSelected] = useState<Option>({
		label: '',
		value: '',
	});

	useEffect(() => {
		const timeout = setTimeout(async () => {
			try {
				const response = await axios.get(
					`http://localhost:8000/api/movies/movieName?title=${title}`
				);
				setOptions(response.data);
				setLoading(false);
			} catch (error) {
				console.error(error);
			}
		}, 300);
		return () => clearTimeout(timeout);
	}, [title]);

	const inputChangeHandler = (input: string) => {
		setTitle(input);
		if (input !== '' && !loading) setLoading(true);
	};

	const selectMovieHandler = (option: SingleValue<Option>) => {
		setLoading(false);
		setSelected(option!!);
		onOpen();
	};

	return (
		<Box mr={5} width='80%' color='black'>
			<Select<Option>
				maxMenuHeight={220}
				menuPlacement='auto'
				styles={{
					control: (base) => ({
						...base,
						'&:hover': {
							cursor: 'text',
						},
					}),
				}}
				options={options}
				value={selected}
				isLoading={loading}
				noOptionsMessage={() => <p>No movies found</p>}
				loadingMessage={({ inputValue }) => <p>Searching for {inputValue}</p>}
				getOptionLabel={(option) => option.label}
				getOptionValue={(option) => option.value}
				onChange={selectMovieHandler}
				onInputChange={inputChangeHandler}
				placeholder='Type to search'
			/>

			<CustomModal
				size='6xl'
				header='Movie'
				isOpen={isOpen}
				onClose={() => {
					setSelected({
						label: '',
						value: '',
					});
					onClose();
				}}
				body={<Movie uuid={selected!!.value} />}
			/>
		</Box>
	);
};

export default SearchBar;

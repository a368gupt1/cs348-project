import {
	Button,
	FormControl,
	FormErrorMessage,
	FormLabel,
	Input,
	Textarea,
} from '@chakra-ui/react';
import { useRef } from 'react';
import useInput from '../../hooks/useInput';
import { useAppDispatch, useAppSelector } from '../../hooks/useRedux';
import { updateProfile } from '../../store/auth/actions';
import CustomModal from '../modal/CustomModal';

interface UpdateProfileProps {
	isOpen: boolean;
	onClose: () => void;
}

const UpdateProfile = ({ isOpen, onClose }: UpdateProfileProps) => {
	const initialRef = useRef(null);
	const dispatch = useAppDispatch();
	const { username, profile } = useAppSelector((state) => state.auth);

	const submitHandler = (e: React.SyntheticEvent) => {
		e.preventDefault();
		if (!inputUsernameIsValid) return;
		dispatch(
			updateProfile({
				username: inputUsername,
				profile: inputProfile,
			})
		);
    onClose();
	};

	const {
		value: inputUsername,
		valueChangeHandler: inputUsernameChangeHandler,
		valueBlurHandler: inputUsernameBlurHandler,
		valueIsInvalid: inputUsernameIsInvalid,
		valueIsValid: inputUsernameIsValid,
	} = useInput((value) => value.trim() !== '', username);
	const {
		value: inputProfile,
		valueChangeHandler: inputProfileChangeHandler,
		valueBlurHandler: inputProfileBlurHandler,
	} = useInput(() => true, profile);

	return (
		<CustomModal
			header='Edit Profile'
			isOpen={isOpen}
			onClose={onClose}
			body={
				<>
					<FormControl isInvalid={inputUsernameIsInvalid} isRequired>
						<FormLabel htmlFor='username'>Username</FormLabel>
						<Input
							id='username'
							type='text'
							value={inputUsername}
							placeholder='username'
							onChange={inputUsernameChangeHandler}
							onBlur={inputUsernameBlurHandler}
						/>
						<FormErrorMessage>Invalid username</FormErrorMessage>
					</FormControl>

					<FormControl mt={4}>
						<FormLabel htmlFor='profile'>Profile</FormLabel>
						<Textarea
							id='profile'
							value={inputProfile}
							onChange={inputProfileChangeHandler}
							onBlur={inputProfileBlurHandler}
							placeholder='A few things about yourself...'
							size='sm'
						/>
					</FormControl>
				</>
			}
			footer={
				<>
					<Button onClick={onClose} mr={3}>
						Cancel
					</Button>
					<Button colorScheme='blue' onClick={submitHandler}>
						Save
					</Button>
				</>
			}
			initialFocusRef={initialRef}
		/>
	);
};

export default UpdateProfile;

import { useColorModeValue } from '@chakra-ui/react';
import { ChartData, ChartOptions } from 'chart.js';
import { Line } from 'react-chartjs-2';
import { MovieData } from '../container/ChartContainer';

interface ChartProps {
	chartData: MovieData;
}

const Chart = ({ chartData }: ChartProps) => {
	const labelColor = useColorModeValue('black', 'white');
	const data: ChartData<'line'> = {
		labels: chartData.labels,
		datasets: [
			{
				label: 'Movies',
				data: chartData.moviesCount,
				fill: false,
				borderColor: 'rgb(75, 192, 192)',
				pointRadius: 8,
				tension: 0.1,
				hidden: false,
			},
			{
				label: 'Average rating',
				data: chartData.avgRating,
				fill: false,
				borderColor: 'rgb(185, 46, 46)',
				pointRadius: 8,
				tension: 0.1,
				hidden: false,
			},
			{
				label: 'Min rating',
				data: chartData.minRating,
				fill: false,
				borderColor: 'rgb(146, 46, 185)',
				pointRadius: 8,
				tension: 0.1,
				hidden: false,
			},
			{
				label: 'Max rating',
				data: chartData.maxRating,
				fill: false,
				borderColor: 'rgb(195, 213, 34)',
				pointRadius: 8,
				tension: 0.1,
				hidden: false,
			},
		],
	};
	const options: ChartOptions<'line'> = {
		scales: {
			y: {
				beginAtZero: true,
				ticks: {
					color: labelColor,
				},
			},
			x: {
				ticks: {
					color: labelColor,
				},
			},
		},
		color: labelColor,
	};

	return <Line data={data} options={options} />;
};

export default Chart;

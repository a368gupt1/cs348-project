import {useEffect} from 'react';
import {ArrowDownIcon, ArrowUpIcon} from '@chakra-ui/icons';
import {Button, Center, Container, Divider, Heading, HStack, Wrap,} from '@chakra-ui/react';
import {useNavigate} from 'react-router-dom';
import InfiniteScroll from 'react-infinite-scroll-component';
import {useAppDispatch, useAppSelector} from '../../hooks/useRedux';
import {MovieListTitles} from '../../types';
import MovieItem from './MovieItem';
import {
	fetchMorePopularMovies,
	fetchMoreSuggestedMovies,
	fetchMoreTrendingMovies,
	getPopularMovies,
	getSuggestedMovies,
	getTrendingMovies,
	getFavoriteMovies,
} from '../../store/movie/actions';
import {updateSortBy} from '../../store/movie/movieSlice';

interface MovieListProps {
	title: MovieListTitles;
}

const MovieList = ({ title }: MovieListProps) => {
	const {
		sort,
		fieldCursor,
		uuidCursor,
		loading,
		movieData: { movies, hasMore },
	} = useAppSelector((state) => state.movie[title]);
	const { token, isAuthenticated } = useAppSelector((state) => state.auth);
	const navigate = useNavigate();
	const dispatch = useAppDispatch();

	const descending = sort === 'DESC';

	useEffect(() => {
		if (title === MovieListTitles.POPULAR) {
			dispatch(getPopularMovies({ sort }));
		} else if (title === MovieListTitles.TRENDING) {
			dispatch(getTrendingMovies({ sort }));
		} else if (title === MovieListTitles.EXPLORE) {
			if (!isAuthenticated) navigate('/');
			dispatch(getSuggestedMovies({ token, sort }));
		} else if (title === MovieListTitles.FAVOURITES) {
			if (!isAuthenticated) navigate('/');
			dispatch(getFavoriteMovies({ token, sort }));
		}
	}, [dispatch, title, sort, token, isAuthenticated, navigate]);

	const fetchMore = () => {
		if (title === MovieListTitles.POPULAR) {
			dispatch(
				fetchMorePopularMovies({
					sort,
					popularity: fieldCursor,
					uuid: uuidCursor,
				})
			);
		} else if (title === MovieListTitles.TRENDING) {
			dispatch(
				fetchMoreTrendingMovies({
					sort,
					ratingAverage: fieldCursor,
					uuid: uuidCursor,
				})
			);
		} else if (title === MovieListTitles.EXPLORE) {
			dispatch(
				fetchMoreSuggestedMovies({
					token,
					sort,
					matchingGenres: fieldCursor,
					uuid: uuidCursor,
				})
			);
		}
	};

	const sortHandler = () => {
		const newSort = descending ? 'ASC' : 'DESC';
		dispatch(updateSortBy({ page: title, sort: newSort }));
	};

	return (
		<Container maxW={'7xl'} p='2'>
			<HStack justifyContent='space-between'>
				<Heading as='h2'>{title}</Heading>
				<Button
					isLoading={loading}
					variant='ghost'
					leftIcon={descending ? <ArrowDownIcon /> : <ArrowUpIcon />}
					onClick={sortHandler}
				>
					{sort}
				</Button>
			</HStack>
			<Divider marginTop='5' />
			<InfiniteScroll
				next={fetchMore}
				hasMore={hasMore}
				dataLength={movies.length}
				loader={
					<Center>
						<Heading fontSize='2xl'>Loading...</Heading>
					</Center>
				}
				endMessage={
					<Center>
						<Heading fontSize='2xl'>Yay! You have seen it all</Heading>
					</Center>
				}
			>
				<Wrap spacing='30px'>
					{movies.map((movie) => (
						<MovieItem
							uuid={movie.uuid}
							key={movie.uuid}
							title={movie.title}
							overview={movie.overview}
							poster={movie.poster}
							backdrop={movie.backdrop}
							genres={movie.genres}
						/>
					))}
				</Wrap>
			</InfiniteScroll>
		</Container>
	);
};

export default MovieList;

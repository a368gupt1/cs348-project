import { useEffect, useState } from 'react';
import axios from 'axios';
import {
	IconButton,
	Box,
	Text,
	useColorModeValue,
	Textarea,
	Button,
	Stack,
} from '@chakra-ui/react';
import ReviewItem from '../review/ReviewItem';
import { AiOutlineStar, AiFillStar } from 'react-icons/ai';
import { useAppSelector } from '../../hooks/useRedux';

interface RatingProps {
	maxRating?: number;
	initialRating?: number;
	movieId: string;
}

const url = 'http://localhost:8000/api/reviews';

const getText = (index: number): String => {
	switch (index) {
		case 0:
			return 'Rate it...';
		case 1:
			return 'Absolutely hated it';
		case 2:
			return 'Hated it';
		case 3:
			return 'Disliked it';
		case 4:
			return 'It was not good';
		case 5:
			return 'It was average';
		case 6:
			return 'It was ok';
		case 7:
			return 'It was interesting';
		case 8:
			return 'Liked it';
		case 9:
			return 'Enjoyed it';
		default:
			return 'Loved it';
	}
};

const Rating = ({
	maxRating = 10,
	initialRating = 0,
	movieId,
}: RatingProps) => {
	const { username, role, isAuthenticated, token } = useAppSelector(
		(state) => state.auth
	);
	const [rating, setRating] = useState(initialRating);
	const [rated, setRated] = useState<Boolean>(false);
	const [text, setText] = useState<String>('Rate it...');
	const [review, setReview] = useState('');
	const [showForm, setShowForm] = useState(false);
	const [reviewed, setReviewed] = useState(false);

	const handleHover = (index: number) => {
		if (!rated && isAuthenticated) {
			setRating(index);
			setText(getText(index));
		}
	};

	const handleClick = (index: number) => {
		if (!rated && isAuthenticated) {
			setRating(index);
			setRated(true);
			setText(getText(index));
			setShowForm(true);
		}
	};

	const submitHandler = async (e: React.SyntheticEvent) => {
		e.preventDefault();
		const body = {
			movieId,
			content: review,
			rating,
		};
		try {
			await axios.post(`${url}/review`, body, {
				headers: {
					'x-auth-token': token,
				},
			});
			setReviewed(true);
		} catch (error) {
			console.error(error);
		}
	};

	// check if user has already reviewed the movie
	useEffect(() => {
		const getReview = async () => {
			const { data } = await axios.get(`${url}/review/${movieId}`, {
				headers: {
					'x-auth-token': token,
				},
			});

			if (data) {
				setReviewed(true);
				setRated(true);
				setRating(data.rating);
				setReview(data.content);
				setText(getText(data.rating));
				setShowForm(true);
			}
		};
		if (isAuthenticated) {
			try {
				getReview();
			} catch (error) {
				console.error(error);
			}
		}
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);

	const iconColor = useColorModeValue('blueviolet', 'yellow');

	return (
		<>
			<Text textAlign='center'>{text}</Text>
			<Box
				mt={2}
				mb={5}
				display='flex'
				alignItems='center'
				justifyContent='space-between'
			>
				{Array.from({ length: maxRating }, (_, i) => (
					<IconButton
						// border='none'
						variant='unstyled'
						color={iconColor}
						key={i}
						aria-label={`Rate ${i + 1} out of ${maxRating}`}
						icon={rating > i ? <AiFillStar /> : <AiOutlineStar />}
						onClick={() => handleClick(i + 1)}
						onPointerOver={() => handleHover(i + 1)}
						onPointerLeave={() => handleHover(0)}
					/>
				))}
			</Box>
			{!isAuthenticated && <Text textAlign='center'>Sign in to review</Text>}
			{showForm && (
				<Stack direction='column' spacing='8'>
					{reviewed ? (
						<ReviewItem
							content={review}
							index={19}
							username={username}
							role={role}
              rating={rating}
						/>
					) : (
						<>
							<Textarea
								value={review}
								onChange={(e: React.ChangeEvent<HTMLTextAreaElement>) =>
									setReview(e.target.value)
								}
								placeholder='Write a review (optional)'
								size='md'
								height='12rem'
							/>
							<Stack alignSelf='center' direction='row' spacing={4}>
								<Button
									colorScheme='gray'
									variant='outline'
									onClick={() => {
										setRating(0);
										setText(getText(0));
										setReview('');
										setShowForm(false);
										setRated(false);
									}}
								>
									Cancel
								</Button>
								<Button
									onClick={submitHandler}
									colorScheme='blue'
									variant='solid'
								>
									Submit
								</Button>
							</Stack>
						</>
					)}
				</Stack>
			)}
		</>
	);
};

export default Rating;

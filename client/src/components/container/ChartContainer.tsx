import { Chart as C, registerables } from 'chart.js';
import SideBar from '../sidebar/SideBar';
import Chart from '../chart/Chart';
import { useEffect, useState } from 'react';
import axios from 'axios';

C.register(...registerables);

export interface MovieData {
	labels: string[];
	moviesCount: number[];
	avgRating: number[];
	minRating: number[];
	maxRating: number[];
}

const ChartContainer = () => {
	const [chartData, setChartData] = useState<MovieData>({
		labels: [],
		moviesCount: [],
		avgRating: [],
		minRating: [],
		maxRating: [],
	});

	useEffect(() => {
		const fetchChartData = async () => {
			const response = await axios.get(
				'http://localhost:8000/api/movies/genres'
			);
			setChartData(response.data);
		};
		try {
			fetchChartData();
		} catch (error) {
			console.error(error);
		}
	}, []);

	return (
		<SideBar>
			<Chart chartData={chartData} />
		</SideBar>
	);
};

export default ChartContainer;

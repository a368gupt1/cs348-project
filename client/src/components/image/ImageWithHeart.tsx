import { Box, Image, IconButton } from '@chakra-ui/react';
import { FaHeart } from 'react-icons/fa';
import { useAppSelector } from '../../hooks/useRedux';
import axios from 'axios';

interface DoubleTapImageProps {
	src: string;
	isLiked: boolean;
	movieUuid: string;
	setIsLiked: React.Dispatch<React.SetStateAction<boolean>>;
}

const DoubleTapImage = ({
	src,
	isLiked,
	setIsLiked,
	movieUuid,
}: DoubleTapImageProps) => {
	const { isAuthenticated, token } = useAppSelector((state) => state.auth);

	const handleLike = async () => {
		if (!isLiked) {
			await axios.post(
				`http://localhost:8000/api/favourites/favourite/${movieUuid}`,
				{},
				{
					headers: {
						'x-auth-token': token,
					},
				}
			);
		} else {
			await axios.delete(
				`http://localhost:8000/api/favourites/favourite/${movieUuid}`,
				{
					headers: {
						'x-auth-token': token,
					},
				}
			);
		}
		setIsLiked(!isLiked);
	};

	return (
		<Box position='relative'>
			<Image
				rounded={'md'}
				alt='image'
				src={src}
				fit='cover'
				objectPosition='center'
				align='center'
				w={'100%'}
				h={{ base: '100%', sm: '400px', lg: '500px' }}
			/>
			{isAuthenticated && (
				<IconButton
					position='absolute'
					top='0'
					right='0'
					bg='none'
					aria-label='Like'
					icon={<FaHeart color={isLiked ? 'red' : 'gray'} />}
					size='lg'
					onClick={handleLike}
				/>
			)}
		</Box>
	);
};
export default DoubleTapImage;

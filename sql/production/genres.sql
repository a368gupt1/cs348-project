/*
-- Query: select * from genres
LIMIT 0, 1000

-- Date: 2023-03-19 23:54
*/
INSERT INTO `` (`uuid`,`name`) VALUES ('33be0031-a77d-4f28-b8bb-b79cb52cf715','Action');
INSERT INTO `` (`uuid`,`name`) VALUES ('6a448211-6f1b-4d90-a9f1-1845e9b77835','Adventure');
INSERT INTO `` (`uuid`,`name`) VALUES ('31e83934-21a9-41bd-b2d1-9ca36b2e8803','Animation');
INSERT INTO `` (`uuid`,`name`) VALUES ('33c3aa11-85fd-4877-a395-9e6ad5f01afe','Comedy');
INSERT INTO `` (`uuid`,`name`) VALUES ('a4f4fc9e-cd3b-43d2-b18d-9de06a58e9c5','Crime');
INSERT INTO `` (`uuid`,`name`) VALUES ('5653239a-e9fe-445e-97f3-7b8e1e54a2c9','Documentary');
INSERT INTO `` (`uuid`,`name`) VALUES ('3eec78cd-4a7b-4a35-902f-d58450acf6c5','Drama');
INSERT INTO `` (`uuid`,`name`) VALUES ('515d6909-2fad-4acc-a698-4ba3c9a6825f','Family');
INSERT INTO `` (`uuid`,`name`) VALUES ('d7ac97c6-078a-48a5-b635-00e5d3668a08','Fantasy');
INSERT INTO `` (`uuid`,`name`) VALUES ('ae10db83-13a9-44b8-9054-73c96a438d29','History');
INSERT INTO `` (`uuid`,`name`) VALUES ('017b0a1a-114c-4859-9fab-dcc558697908','Horror');
INSERT INTO `` (`uuid`,`name`) VALUES ('d2ce1a67-621d-4b65-a0c7-1003b56f4efc','Music');
INSERT INTO `` (`uuid`,`name`) VALUES ('79868766-1a09-4646-bddd-5aece0ac1513','Mystery');
INSERT INTO `` (`uuid`,`name`) VALUES ('bc5121ae-1bd5-44c9-86f2-48902ec7bb1c','Romance');
INSERT INTO `` (`uuid`,`name`) VALUES ('60c1330e-9027-4216-bd84-e07fb897aaa4','Science Fiction');
INSERT INTO `` (`uuid`,`name`) VALUES ('c8d28872-8a6b-4aef-8133-2cc245932a66','Thriller');
INSERT INTO `` (`uuid`,`name`) VALUES ('16160812-bd07-4acc-828e-f35e9e72cd4d','TV Movie');
INSERT INTO `` (`uuid`,`name`) VALUES ('28e78203-2242-4a43-94ec-e81cf4b3e255','War');
INSERT INTO `` (`uuid`,`name`) VALUES ('9d578ec8-bbbb-473f-8fa2-17e6d289d89d','Western');

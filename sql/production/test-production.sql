-- Get popular movies
WITH temp AS (
  SELECT *
  FROM movies
  ORDER BY popularity DESC,
    uuid DESC
  LIMIT 19
)
SELECT temp.uuid,
  temp.title,
  temp.overview,
  temp.poster,
  temp.backdrop,
  temp.adult,
  temp.releasedDate,
  temp.runtime,
  temp.ratingCount,
  temp.ratingAverage,
  temp.popularity,
  genres.name,
  movieGenres.GenreUuid
FROM temp
  LEFT JOIN movieGenres ON movieGenres.movieUuid = temp.uuid
  LEFT JOIN genres ON movieGenres.GenreUuid = genres.uuid
ORDER BY popularity DESC,
  uuid DESC;
-- Get popular movies - pagination
WITH temp AS (
  SELECT *
  FROM movies
  WHERE popularity < '747'
    OR (
      popularity = '747'
      AND uuid < '006d3b03-08e8-468f-ae8d-8aa6ade10b9b'
    )
  ORDER BY popularity DESC,
    uuid DESC
  LIMIT 19
)
SELECT temp.uuid,
  temp.title,
  temp.overview,
  temp.poster,
  temp.backdrop,
  temp.adult,
  temp.releasedDate,
  temp.runtime,
  temp.ratingCount,
  temp.ratingAverage,
  temp.popularity,
  genres.name,
  movieGenres.GenreUuid
FROM temp
  LEFT JOIN movieGenres ON movieGenres.movieUuid = temp.uuid
  LEFT JOIN genres ON movieGenres.GenreUuid = genres.uuid
ORDER BY popularity DESC,
  uuid DESC;
-- Register
INSERT INTO users (uuid, username, email, role, profile, password)
VALUES (
    'c8710c02-59d8-44a8-bb05-a062d052098f',
    'John Doe',
    'john@gmail.com',
    'Admin',
    'Hello world',
    '$2a$10$REJaLKkZP3CLbsvBNybL7eRO2jrN4yThNPOAd/By2PjFbpdec2ak6'
  );
-- Add movie to favourites
INSERT INTO favourites(userUuid, movieUuid)
VALUES (
    'c8710c02-59d8-44a8-bb05-a062d052098f',
    '32c116a8-4963-4e4f-be47-524832afe97e'
  );
-- Remove movie from favourites
INSERT INTO favourites(userUuid, movieUuid)
VALUES (
    'c8710c02-59d8-44a8-bb05-a062d052098f',
    '7f67121b-28c9-4b2f-80dc-e0edc879d095'
  );
DELETE FROM favourites
WHERE userUuid = 'c8710c02-59d8-44a8-bb05-a062d052098f'
  AND movieUuid = '7f67121b-28c9-4b2f-80dc-e0edc879d095';
-- Update user profile
UPDATE users
SET profile = 'Hello world',
  username = 'John'
WHERE uuid = 'c8710c02-59d8-44a8-bb05-a062d052098f';
-- Find movies by title
SELECT uuid AS value,
  title AS label
FROM movies
WHERE LOWER(title) LIKE '%titanic%'
LIMIT 19;
-- Review movie
INSERT INTO reviews (uuid, content, rating, userUuid, movieUuid)
VALUES (
    '25f9818d-646c-4a37-9492-f4df51a68c24',
    'Nice movie',
    10,
    'c8710c02-59d8-44a8-bb05-a062d052098f',
    '7f67121b-28c9-4b2f-80dc-e0edc879d095'
  );
-- Get movie reviews
SELECT *
FROM reviews
WHERE movieUuid = '7f67121b-28c9-4b2f-80dc-e0edc879d095';
-- Get favourite movies
WITH favouriteMovies AS (
  SELECT movies.uuid,
    movies.title,
    movies.overview,
    movies.poster,
    movies.backdrop,
    movies.adult,
    movies.releasedDate,
    movies.runtime,
    movies.ratingCount,
    movies.ratingAverage,
    movies.popularity
  FROM movies
    JOIN favourites ON movies.uuid = favourites.movieUuid
    AND favourites.userUuid = 'c8710c02-59d8-44a8-bb05-a062d052098f'
),
temp AS (
  SELECT *
  FROM favouriteMovies
  ORDER BY popularity DESC,
    uuid DESC
  LIMIT 19
)
SELECT temp.uuid,
  temp.title,
  temp.overview,
  temp.poster,
  temp.backdrop,
  temp.adult,
  temp.releasedDate,
  temp.runtime,
  temp.ratingCount,
  temp.ratingAverage,
  temp.popularity,
  genres.name,
  movieGenres.GenreUuid
FROM temp
  LEFT JOIN movieGenres ON movieGenres.movieUuid = temp.uuid
  LEFT JOIN genres ON movieGenres.GenreUuid = genres.uuid
ORDER BY popularity DESC,
  uuid DESC;
-- Get genre statistics
SELECT name,
  AVG(ratingAverage) as avgRating,
  MIN(ratingAverage) as minRating,
  MAX(ratingAverage) as maxRating,
  COUNT(*) as moviesCount
FROM genres
  JOIN movieGenres ON genres.uuid = movieGenres.genreUuid
  JOIN movies ON movies.uuid = movieGenres.movieUuid
GROUP BY genres.uuid
ORDER BY name ASC;
-- Get recommended movies
WITH userFavouriteGenres AS (
  SELECT genreUuid
  FROM favourites
    JOIN movieGenres ON favourites.movieUuid = movieGenres.movieUuid
  WHERE favourites.userUuid = 'c8710c02-59d8-44a8-bb05-a062d052098f'
),
suggestedMovies AS (
  SELECT movies.*,
    COUNT(DISTINCT movieGenres.genreUuid) AS matchingGenres
  FROM movies
    JOIN movieGenres ON movies.uuid = movieGenres.movieUuid
  WHERE movies.uuid NOT IN (
      SELECT movieUuid
      FROM favourites
      WHERE userUuid = 'c8710c02-59d8-44a8-bb05-a062d052098f'
    )
    AND movieGenres.genreUuid IN (
      SELECT genreUuid
      FROM userFavouriteGenres
    )
  GROUP BY movies.uuid
  ORDER BY matchingGenres DESC,
    uuid DESC
  LIMIT 19
)
SELECT suggestedMovies.uuid,
  suggestedMovies.title,
  suggestedMovies.overview,
  suggestedMovies.poster,
  suggestedMovies.backdrop,
  suggestedMovies.adult,
  suggestedMovies.releasedDate,
  suggestedMovies.runtime,
  suggestedMovies.ratingCount,
  suggestedMovies.ratingAverage,
  suggestedMovies.popularity,
  suggestedMovies.matchingGenres,
  genres.name,
  movieGenres.GenreUuid
FROM suggestedMovies
  JOIN movieGenres ON suggestedMovies.uuid = movieGenres.movieUuid
  JOIN genres ON genres.uuid = movieGenres.genreUuid
ORDER BY matchingGenres DESC,
  uuid DESC;
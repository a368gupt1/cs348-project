-- Get popular movies
WITH temp AS (
  SELECT *
  FROM movies
  ORDER BY popularity DESC,
    uuid DESC
  LIMIT 19
)
SELECT temp.uuid,
  temp.title,
  temp.overview,
  temp.poster,
  temp.backdrop,
  temp.adult,
  temp.releasedDate,
  temp.runtime,
  temp.ratingCount,
  temp.ratingAverage,
  temp.popularity,
  genres.name,
  movieGenres.GenreUuid
FROM temp
  LEFT JOIN movieGenres ON movieGenres.movieUuid = temp.uuid
  LEFT JOIN genres ON movieGenres.GenreUuid = genres.uuid
ORDER BY popularity DESC,
  uuid DESC;
-- Get popular movies - pagination
WITH temp AS (
  SELECT *
  FROM movies
  WHERE popularity < '747'
    OR (
      popularity = '747'
      AND uuid < 'e3f04338-7864-49b4-a541-a0981b4ce279'
    )
  ORDER BY popularity DESC,
    uuid DESC
  LIMIT 19
)
SELECT temp.uuid,
  temp.title,
  temp.overview,
  temp.poster,
  temp.backdrop,
  temp.adult,
  temp.releasedDate,
  temp.runtime,
  temp.ratingCount,
  temp.ratingAverage,
  temp.popularity,
  genres.name,
  movieGenres.GenreUuid
FROM temp
  LEFT JOIN movieGenres ON movieGenres.movieUuid = temp.uuid
  LEFT JOIN genres ON movieGenres.GenreUuid = genres.uuid
ORDER BY popularity DESC,
  uuid DESC;
-- Register
INSERT INTO users (uuid, username, email, role, profile, password)
VALUES (
    'aa6fd190-aeba-4537-9552-db4f4d6b591f',
    'John Doe',
    'john@gmail.com',
    'Admin',
    'Hello world',
    '$2a$10$1enbLFevVSpXjut44.d6q.RD6BEGfGany0b60wYOoJAtPOqL7ZniG'
  );
-- Add movie to favourites
INSERT INTO favourites(userUuid, movieUuid)
VALUES (
    'aa6fd190-aeba-4537-9552-db4f4d6b591f',
    '2e2db66a-a1b3-421b-98c2-8cb7c1af019c'
  );
-- Remove movie from favourites
INSERT INTO favourites(userUuid, movieUuid)
VALUES (
    'aa6fd190-aeba-4537-9552-db4f4d6b591f',
    'b060303c-80ca-4bfc-a545-742ebcf5fd05'
  );
DELETE FROM favourites
WHERE userUuid = 'aa6fd190-aeba-4537-9552-db4f4d6b591f'
  AND movieUuid = 'b060303c-80ca-4bfc-a545-742ebcf5fd05';
-- Update user profile
UPDATE users
SET profile = 'Hello world',
  username = 'John'
WHERE uuid = 'aa6fd190-aeba-4537-9552-db4f4d6b591f';
-- Find movies by title
SELECT uuid AS value,
  title AS label
FROM movies
WHERE LOWER(title) LIKE '%titanic%'
LIMIT 19;
-- Review movie
INSERT INTO reviews (uuid, content, rating, userUuid, movieUuid)
VALUES (
    '25f9818d-646c-4a37-9492-f4df51a68c24',
    'Nice movie',
    10,
    'aa6fd190-aeba-4537-9552-db4f4d6b591f',
    'b060303c-80ca-4bfc-a545-742ebcf5fd05'
  );
-- Get movie reviews
SELECT *
FROM reviews
WHERE movieUuid = 'b060303c-80ca-4bfc-a545-742ebcf5fd05';
-- Get favourite movies
WITH favouriteMovies AS (
  SELECT movies.uuid,
    movies.title,
    movies.overview,
    movies.poster,
    movies.backdrop,
    movies.adult,
    movies.releasedDate,
    movies.runtime,
    movies.ratingCount,
    movies.ratingAverage,
    movies.popularity
  FROM movies
    JOIN favourites ON movies.uuid = favourites.movieUuid
    AND favourites.userUuid = 'aa6fd190-aeba-4537-9552-db4f4d6b591f'
),
temp AS (
  SELECT *
  FROM favouriteMovies
  ORDER BY popularity DESC,
    uuid DESC
  LIMIT 19
)
SELECT temp.uuid,
  temp.title,
  temp.overview,
  temp.poster,
  temp.backdrop,
  temp.adult,
  temp.releasedDate,
  temp.runtime,
  temp.ratingCount,
  temp.ratingAverage,
  temp.popularity,
  genres.name,
  movieGenres.GenreUuid
FROM temp
  LEFT JOIN movieGenres ON movieGenres.movieUuid = temp.uuid
  LEFT JOIN genres ON movieGenres.GenreUuid = genres.uuid
ORDER BY popularity DESC,
  uuid DESC;
-- Get genre statistics
SELECT name,
  AVG(ratingAverage) as avgRating,
  MIN(ratingAverage) as minRating,
  MAX(ratingAverage) as maxRating,
  COUNT(*) as moviesCount
FROM genres
  JOIN movieGenres ON genres.uuid = movieGenres.genreUuid
  JOIN movies ON movies.uuid = movieGenres.movieUuid
GROUP BY genres.uuid
ORDER BY name ASC;
-- Get recommended movies
WITH userFavouriteGenres AS (
  SELECT genreUuid
  FROM favourites
    JOIN movieGenres ON favourites.movieUuid = movieGenres.movieUuid
  WHERE favourites.userUuid = 'aa6fd190-aeba-4537-9552-db4f4d6b591f'
),
suggestedMovies AS (
  SELECT movies.*,
    COUNT(DISTINCT movieGenres.genreUuid) AS matchingGenres
  FROM movies
    JOIN movieGenres ON movies.uuid = movieGenres.movieUuid
  WHERE movies.uuid NOT IN (
      SELECT movieUuid
      FROM favourites
      WHERE userUuid = 'aa6fd190-aeba-4537-9552-db4f4d6b591f'
    )
    AND movieGenres.genreUuid IN (
      SELECT genreUuid
      FROM userFavouriteGenres
    )
  GROUP BY movies.uuid
  ORDER BY matchingGenres DESC,
    uuid DESC
  LIMIT 19
)
SELECT suggestedMovies.uuid,
  suggestedMovies.title,
  suggestedMovies.overview,
  suggestedMovies.poster,
  suggestedMovies.backdrop,
  suggestedMovies.adult,
  suggestedMovies.releasedDate,
  suggestedMovies.runtime,
  suggestedMovies.ratingCount,
  suggestedMovies.ratingAverage,
  suggestedMovies.popularity,
  suggestedMovies.matchingGenres,
  genres.name,
  movieGenres.GenreUuid
FROM suggestedMovies
  JOIN movieGenres ON suggestedMovies.uuid = movieGenres.movieUuid
  JOIN genres ON genres.uuid = movieGenres.genreUuid
ORDER BY matchingGenres DESC,
  uuid DESC;
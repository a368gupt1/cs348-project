/*
-- Query: select * from genres
LIMIT 0, 1000

-- Date: 2023-03-05 00:55
*/
INSERT INTO `` (`uuid`,`name`) VALUES ('25dce479-ef76-46ee-bf1c-c9ede8cd72ee','Action');
INSERT INTO `` (`uuid`,`name`) VALUES ('56e412f0-e06f-4132-90b5-3cd7677574c9','Adventure');
INSERT INTO `` (`uuid`,`name`) VALUES ('835de27e-569f-41fa-ac61-dcb9a4c26d73','Animation');
INSERT INTO `` (`uuid`,`name`) VALUES ('773bce4b-e7fc-4d4a-8907-7b71596b12f4','Comedy');
INSERT INTO `` (`uuid`,`name`) VALUES ('c3329bde-62ce-4186-8323-16213464002e','Crime');
INSERT INTO `` (`uuid`,`name`) VALUES ('34f86a8f-4217-4069-8afa-da68360ba838','Documentary');
INSERT INTO `` (`uuid`,`name`) VALUES ('eb1255ed-5eec-48d2-b4c5-b492ff13e56a','Drama');
INSERT INTO `` (`uuid`,`name`) VALUES ('4c66bb3e-ebc9-407e-a107-b9acfd239c81','Family');
INSERT INTO `` (`uuid`,`name`) VALUES ('8cffab79-f8b4-4738-922d-db7eec265144','Fantasy');
INSERT INTO `` (`uuid`,`name`) VALUES ('28a992cf-37db-428b-acff-6381e0438a30','History');
INSERT INTO `` (`uuid`,`name`) VALUES ('4702af58-95cf-467c-8ba2-216f5ddac956','Horror');
INSERT INTO `` (`uuid`,`name`) VALUES ('1c05740c-85d7-4ea0-a84e-749cf5781f83','Music');
INSERT INTO `` (`uuid`,`name`) VALUES ('067a6a95-284c-41f8-844f-0b73fbd1892c','Mystery');
INSERT INTO `` (`uuid`,`name`) VALUES ('4b929c10-2694-44d4-86c6-4b579600730e','Romance');
INSERT INTO `` (`uuid`,`name`) VALUES ('c92ef907-2b8e-46b5-9114-33851896200f','Science Fiction');
INSERT INTO `` (`uuid`,`name`) VALUES ('6671c4a0-e0eb-4216-9002-272c1ad29844','Thriller');
INSERT INTO `` (`uuid`,`name`) VALUES ('ebf9025e-cdee-40e5-a24b-92ef81673eb2','TV Movie');
INSERT INTO `` (`uuid`,`name`) VALUES ('bff441b9-e9b9-491b-bdcd-54a0abc2b287','War');
INSERT INTO `` (`uuid`,`name`) VALUES ('8727d115-0062-434e-817c-5f299d2ebb6a','Western');

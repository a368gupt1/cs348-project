const { sequelize } = require('../models');
const { createFavourites, dropFavourites } = require('../models/favourite');
const { createGenres, dropGenres, genreIndex } = require('../models/genre');
const {
	createMovies,
	dropMovies,
	movieTitleIndex,
	moviePopularityIndex,
	movieRatingAverageIndex,
	insertReviewTrigger,
	deleteReviewTrigger,
} = require('../models/movie');
const { createMovieGenres, dropMovieGenres } = require('../models/movieGenres');
const { createReviews, dropReviews } = require('../models/review');
const { createUsers, dropUsers } = require('../models/user');

const syncDatabase = async () => {
	try {
		await sequelize.query('SET FOREIGN_KEY_CHECKS = 0;');

		await sequelize.query(dropFavourites);
		await sequelize.query(dropMovieGenres);
		await sequelize.query(dropReviews);
		await sequelize.query(dropGenres);
		await sequelize.query(dropMovies);
		await sequelize.query(dropUsers);

		await sequelize.query(createGenres);
		await sequelize.query(createMovies);
		await sequelize.query(createUsers);

		await sequelize.query(createFavourites);
		await sequelize.query(createMovieGenres);
		await sequelize.query(createReviews);

		await sequelize.query(movieTitleIndex);
		await sequelize.query(moviePopularityIndex);
		await sequelize.query(movieRatingAverageIndex);
		await sequelize.query(insertReviewTrigger);
		await sequelize.query(deleteReviewTrigger);
		await sequelize.query(genreIndex);

		await sequelize.query('SET FOREIGN_KEY_CHECKS = 1;');
		console.log('Database synchronised');
	} catch (error) {
		throw new Error(error.message);
	}
};

module.exports = {
	syncDatabase,
};

require('dotenv').config();
const fs = require('fs');
const { sequelize } = require('../models');
const { syncDatabase } = require('./util');
const { insertMovie } = require('../services/movieService');
const { insertGenres, getGenreByName } = require('../services/genreService');
const { insertMovieGenre } = require('../services/movieGenreService');

sequelize
	.authenticate()
	.then(async () => {
		await syncDatabase();
		let genres = JSON.parse(fs.readFileSync('./data/genres.json', 'utf8'));
		let movies;
		if (process.env.TYPE === 'dev') {
			movies = JSON.parse(fs.readFileSync('./data/movies.json', 'utf8'));
		} else if (process.env.TYPE === 'prod') {
			movies = JSON.parse(
				fs.readFileSync('./data/movies-production.json', 'utf8')
			);
		}
		await insertGenres(genres);
		// await insertMovies(movies);
		// much slower than before, but well :v
		for (const movie of movies) {
			const createdMovie = await insertMovie(movie);
			for (const genre of movie.genres) {
				const foundGenre = await getGenreByName(genre);
				await insertMovieGenre({
					movieUuid: createdMovie.uuid,
					genreUuid: foundGenre.uuid,
				});
			}
		}
		console.log('Populated database');
	})
	.catch((error) => {
		console.error(error);
		throw new Error(error.message);
	});
